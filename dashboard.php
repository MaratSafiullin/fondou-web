<?php
include_once 'fondou_app/common/error_handler.php';
include_once 'fondou_app/common/authorization.php';
successfulResponse();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:/login.php");
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>fond(ou) Admin - Menu</title>

        <link rel="icon" href="/assets/img/favicon.ico">
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet"> <!-- Bootstrap Core CSS -->
        <link href="/assets/css/sb-admin.css" rel="stylesheet"> <!-- Custom CSS -->
        <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> <!-- Custom Fonts -->

        <!-- Include AngularJS library -->
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
        <!-- Include the AngularJS routing library -->
        <script src="https://code.angularjs.org/1.2.28/angular-route.min.js"></script>
        <!-- Include Angular UI Bootstrap -->
        <script src="/fondou_app/lib/ui-bootstrap-tpls-1.3.3.js"></script>
        <!-- File uploading lib -->
        <script src="/fondou_app/oi/oi.file.js"></script>
        <!-- Common JS functions lib -->
        <script src="/fondou_app/lib/CommonFunctions.js"></script>

    </head>
    <body ng-app="fondou" ng-controller="MainController">
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">fond(ou) Admin</a>
                </div>

                <!-- L o g o u t -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul> <!-- L o g o u t -->

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <!-- O R D E R S -->
                        <li> <a href="/dashboard/#/orders/">Orders</a> </li>

                        <!-- M E N U -->
                        <li> <a href="/dashboard/#/menu/">Menu</a> </li>

                        <!-- S P E C I A L S -->
                        <li> <a href="/dashboard/#/specials/">Specials</a> </li>

                        <!-- O R D E R S  A R C H I V E -->
                        <li> <a href="/dashboard/#/orders-archive/">Orders Archive</a> </li>

                        <!-- M E N U  O P T I O N S -->
                        <li> <a href="/dashboard/#/options/">Menu Options</a> </li>

                        <!-- C U S T O M E R S -->
                        <li> <a href="/dashboard/#/customers/">Customers</a> </li>

                        <!-- S E T T I N G S -->
                        <li> <a href="/dashboard/#/settings/">Settings</a> </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <!-- Dashboard page AngularJS view -->
            <div ng-view></div>

        </div>
        <!-- /#wrapper -->

        <script src="/assets/js/jquery.js"></script>  <!-- jQuery -->
        <script src="/assets/js/bootstrap.min.js"></script> <!-- Bootstrap Core JavaScript -->
        
        <!-- Modules -->
        <script src="/fondou_app/fondou.js"></script>

        <!-- Controllers -->
        <script src="/fondou_app/common/MainController.js"></script>
        <script src="/fondou_app/common/ConfirmationController.js"></script>
        <script src="/fondou_app/common/ErrorMessageController.js"></script>

        <script src="/fondou_app/menu/MenuController.js"></script>
        <script src="/fondou_app/options/OptionsController.js"></script>
        <script src="/fondou_app/orders/OrdersController.js"></script>
        <script src="/fondou_app/orders/OrdersArchiveController.js"></script>
        <script src="/fondou_app/specials/SpecialsController.js"></script>
        <script src="/fondou_app/customers/CustomersController.js"></script>
        <script src="/fondou_app/settings/SettingsController.js"></script>


        <!-- Directives -->
        <script src="/fondou_app/menu/ImageUpdoalderDirective.js"></script>
    </body>
</html>
