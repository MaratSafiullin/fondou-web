<?php

/**
 * Class to handle menu items images
 */
class ItemsImagesHandler {

    private $headers;
    private $files;
    // Default params
    private $prm = array(
        'files' => array(
            //First element - original image options
            array( 
                'dir' => '../../img/',
                'fit' => 'cover',
                'width' => 160,
                'height' => 160,
                'ext' => 'jpg',
                'quality' => 75,
                'copyright' => false,
                'w_pct' => 1,
                'h_pct' => 1,
                'x_pct' => null,
                'y_pct' => null
            )
        ),
        'maxSize' => '10M', // Max file size, 0 - unlimited
        'allowedType' => array('jpeg', 'jpg', 'png', 'gif') // Allowed extentions
    );

    //constructor
    public function __construct($param, $headers, $files) {
        //HTTP headers from calling script
        $this->headers = $headers;
        //files array
        $this->files = $files;
        
        //replase default options with options from constructor
        foreach ($param as $k => $v) {
            $this->prm[$k] = $v;
        }
        
        //convert size value
        $this->prm['maxSize'] = $this->toBytes($this->prm['maxSize']);
    }

    /**
     * Return settings
     * @return Settings array
     */
    public function settings() {

        return array(
            'files' => $this->prm['files'],
            'maxSize' => $this->prm['maxSize'],
            'allowedType' => $this->prm['allowedType'],
            'space' => $this->checkSpace(),
            'errorBadType' => sprintf('You can upload: %s', strtoupper(implode(', ', $this->prm['allowedType']))),
            'errorBigSize' => sprintf('Max file size is %s MB', round($this->prm['maxSize'] / 1048576, 2)),
        );
    }

    /**
     * Delete image files for given menu item ID 
     * @param $id Menu item ID
     */
    public function delete($id) {
        //ignore exceptions deleting the file
        try {
            foreach ($this->prm['files'] as $file) {
                foreach (glob($file['dir'] . $id . '_*.*') as $file) {
                    unlink($file);
                }
            }
        } catch (Exception $e) {
            //DO NOTHING
        }
    }

    /**
     * Save (update) image file for menu item
     * @param $id Menu item ID
     * @param $timestamp File loading timestamp (used to set file name) 
     * @return 
     */
    public function update($id, $timestamp) {
        //check file size
        if ((int) $this->headers['Content-Length'] > $this->prm['maxSize']) {
            throw new Exception('File max size exceeded', 5);
        }

        if (!empty($this->files)) {
            // Save file on server. $this->files array should contain only one element
            // in case of exception delete file and rethrow it
            try {
                list($original_name, $ext, $file_size, $file_type) = $this->saveFile($this->files[key($this->files)], $this->prm['files'][0]['dir'], $this->prm['maxSize'], $this->prm['allowedType'], $id, $timestamp);
            } catch (Exception $e) {
                $this->delete($id);
                throw $e;
            }

            $result = array();

            //process file if it's an image
            //make thumbnails out of original file, convert original to app's specific format
            if (in_array($ext, array('jpeg', 'jpg', 'png', 'gif'))) {
                //Load graphics lib
                include_once('../lib/gd.php');

                //create file's versions (thumbnails) before processing the original file
                $fileParamsRev = array_reverse($this->prm['files']);

                //generate and save new images, original file will be replaced with processed version
                foreach ($fileParamsRev as $fileParams) {
                    $result[$fileParams['field']] = Image::edit(array_merge($fileParams, array(
                                'file_input' => $this->prm['files'][0]['dir'] . $original_name,
                                'file_output' => $fileParams['dir'] . $original_name)));

                    $result[$fileParams['field']] = $fileParams['dir'] . $result[$fileParams['field']];

                    if (!$result[$fileParams['field']])
                        throw new Exception('Image processing unsuccesful', 16);
                }
            } else {
                $this->delete($id);
            }

            return $result;
        }
    }

    /**
     * Save file on server
     * Throws extention if conditions are violated or saving unsuccessful
     * @param $file Element from files array
     * @param $fileDir Path to save file
     * @param $maxSize Max file size
     * @param $allowedType Array of allowed file extentions
     * @param $id Menu item ID (for file name)
     * @param $timestamp Loading timestamp (for file name)
     * @return Array of saved file info
     */
    public function saveFile($file, $fileDir, $maxSize, $allowedType, $id, $timestamp) {

        //check for uploading errors
        if ($file['error']) {
            throw new Exception('Unsucessfull upload to $_FILES', $file['error']);
        }
        //check file size
        if ($maxSize && $file['size'] > $maxSize) {
            throw new Exception('File max size exceeded', 12);
        }

        //Get file type from extention or MIME type 
        if (!($ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION))))
            $ext = preg_replace('/^.*([^\/]*)$/U', '$1', $file['type']);
        //check for valid extention
        if (!in_array($ext, $allowedType)) {
            throw new Exception('File type is not allowed', 10);
        }

        //Generate new file name to match menu item and save file
        $new_file_name = $id . '_' . $timestamp . '.' . $ext;
        if (!move_uploaded_file($file['tmp_name'], $fileDir . $new_file_name)) {
            throw new Exception('Unable to move file from temp folder', 9);
        }

        return array($new_file_name, $ext, $file['size'], $file['type'], $file['name']);
    }

    /**
     * Convert file size string (with K(kilo), M(mega), G(giga)) into bytes integer
     * @param $val Size as string
     * @return Size as bytes integer value
     */
    public function toBytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val) - 1]);
        switch ($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

}
