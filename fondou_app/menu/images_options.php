<?php

//menu images options for the app
        const ORIGINALS_PATH = '/img/menu_items/originals/', THUMBNAILS_PATH = '/img/menu_items/thumbnails/',
        DEFAULT_THUMBNAIL_PATH = '/img/menu_items/thumbnails/default.png';
        const ORIGINALS_EXT = 'jpeg', THUMBNAILS_EXT = 'png';

//options for GD lib
$imagesOptions = array(
    'files' => array(
        array('field' => 'original', 'dir' => '../../img/menu_items/originals/', 'fit' => true, 'width' => 1200, 'height' => 1200, 'ext' => 'jpg'),
        array('field' => 'thumb', 'dir' => '../../img/menu_items/thumbnails/', 'fit' => 'cover', 'width' => 160, 'height' => 160, 'ext' => 'png')
    ),
    'maxSize' => '4M',
    'maxSpace' => '1G',
    'allowedType' => array('jpeg', 'jpg', 'png', 'gif')
);
