//Controller for menu page
app.controller('MenuController', ['$scope', '$http', function ($scope, $http) {
        //selected category
        $scope.currentCategory = null;
        //all categories list
        $scope.menuCategories = [];
        //items for selected category
        $scope.menuItems = [];
        //possible item sizes
        $scope.sizes = [];

        //***********************************************************************

        //load menu categories
        $scope.loadCategories = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'ReadCategories'
            })
                    .success(function (menuCategories) {
                        $scope.menuCategories = menuCategories;
                        //select first category in list if there is any
                        if (menuCategories.length > 0) {
                            $scope.currentCategory = menuCategories[0];
                            //load items 
                            $scope.loadItems();
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot load list of categories'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //create new menu category, add to categories list
        $scope.createCategory = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'CreateCategory',
                category_name: ''
            })
                    .success(function (newCategory) {
                        $scope.menuCategories.push(newCategory);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot create new category'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update menu category, show result
        $scope.updateCategory = function (index) {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'UpdateCategory',
                category_id: $scope.menuCategories[index].id,
                category_name: $scope.menuCategories[index].name,
                ordering_simple: $scope.menuCategories[index].orderingSimple,
                ordering_advanced: $scope.menuCategories[index].orderingAdvanced
            })
                    .success(function (category) {
                        $scope.menuCategories[index] = category;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot save category'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete menu category if possible, remove from categories list
        $scope.deleteCategory = function (index) {
            //check if can be deleted
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'CheckCategoryDeletable',
                category_id: $scope.menuCategories[index].id
            })
                    .success(function (result) {
                        //if can be deleted ask for confirmation
                        if (result === "YES") {
                            dialogParameters = {
                                header: 'Delete Category',
                                message: 'Delete Category "' + $scope.menuCategories[index].name + '"?',
                                isDoable: true
                            };
                        }
                        //if not - notify user
                        else {
                            dialogParameters = {
                                header: 'Delete Category',
                                message: 'Category "' + $scope.menuCategories[index].name + '" has items or options. It cannot be deleted',
                                isDoable: false
                            };
                        }

                        //show confirmation dialog
                        dialog = $scope.confirmationDialog(dialogParameters);

                        //delete if confirmed
                        dialog.result.then(function () {
                            $http.post("/fondou_app/menu/menu_manager.php", {
                                action: 'DeleteCategory',
                                category_id: $scope.menuCategories[index].id
                            })
                                    .success(function (result) {
                                        if (result === "SUCCESS") {
                                            $scope.menuCategories.splice(index, 1);
                                        }
                                        //select first category in list if there is any
                                        if ($scope.menuCategories.length > 0) {
                                            $scope.currentCategory = $scope.menuCategories[0];
                                            //load items 
                                            $scope.loadItems();
                                        }
                                    })
                                    .error(function (err) {
                                        dialogParameters = {
                                            header: 'Category Error',
                                            message: 'Cannot delete category'
                                        };
                                        $scope.errorMessageDialog(dialogParameters);
                                    });
                        });
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot check if category can be deleted'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //***********************************************************************

        //load menu items for selected category
        $scope.loadItems = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'ReadItems',
                category_id: $scope.currentCategory.id
            })
                    .success(function (menuItems) {
                        $scope.menuItems = menuItems;
                        //find matching size object for every item
                        for (i = 0; i < menuItems.length; i++) {
                            $scope.findSizeForMenuItem(menuItems[i]);
                        }
                        //
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Menu Item Error',
                            message: 'Cannot load list of items'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //create new menu item of selected category, add to items list
        $scope.createItem = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'CreateItem',
                category_id: $scope.currentCategory.id,
                item_name: '',
                item_description: '',
                item_price: 0,
                size_id: 0
            })
                    .success(function (newItem) {
                        //find matching size object for menu item
                        $scope.findSizeForMenuItem(newItem);
                        //
                        $scope.menuItems.push(newItem);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Menu Item Error',
                            message: 'Cannot create new item'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update menu item, show result
        $scope.updateItem = function (index) {
            var sizeId = 0;
            if ($scope.menuItems[index].size !== null) {
                sizeId = $scope.menuItems[index].size.order;
            }
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'UpdateItem',
                item_id: $scope.menuItems[index].id,
                item_name: $scope.menuItems[index].name,
                item_description: $scope.menuItems[index].description,
                item_price: $scope.menuItems[index].price,
                size_id: sizeId
            })
                    .success(function (item) {
                        //find matching size object for menu item
                        $scope.findSizeForMenuItem(item);
                        //
                        $scope.menuItems[index] = item;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Menu Item Error',
                            message: 'Cannot save item'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update menu item - set "active state", show result
        $scope.setItemActive = function (index) {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'SetItemActive',
                item_id: $scope.menuItems[index].id,
                item_active: $scope.menuItems[index].active
            })
                    .success(function (item) {
                        //find matching size object for menu item
                        $scope.findSizeForMenuItem(item);
                        //
                        $scope.menuItems[index] = item;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Menu Item Error',
                            message: 'Cannot change item`s "active" status'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete menu item, remove from items list
        $scope.deleteItem = function (index) {
            dialogParameters = {
                header: 'Delete Item',
                message: 'Delete Item "' + $scope.menuItems[index].name + '"?',
                isDoable: true
            };

            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);

            //delete if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/menu/menu_manager.php", {
                    action: 'DeleteItem',
                    item_id: $scope.menuItems[index].id
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.menuItems.splice(index, 1);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Menu Item Error',
                                message: 'Cannot delete item'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //load possible menu items sizes
        $scope.loadSizes = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'ReadSizes'
            })
                    .success(function (sizes) {
                        $scope.sizes = sizes;
                        //find matching size object for every item
                        for (i = 0; i < $scope.menuItems.length; i++) {
                            $scope.findSizeForMenuItem($scope.menuItems[i]);
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Size Error',
                            message: 'Cannot load list of sizes'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //find matching size object for menu item
        //used to handle size selection "<select>"
        $scope.findSizeForMenuItem = function (item) {
            for (j = 0; j < $scope.sizes.length; j++) {
                if ($scope.sizes[j].order === item.sizeId) {
                    item.size = $scope.sizes[j];
                    break;
                }
            }
        };

        //***********************************************************************

        //initialize page
        $scope.loadCategories();
        $scope.loadSizes();

    }]);