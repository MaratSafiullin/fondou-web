<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './images_options.php';
include_once './items_images_handler.php';
include_once './items_model.php';

//prepare to work with data model and images
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$itemsModel = new ItemsModel(THUMBNAILS_PATH, ORIGINALS_PATH, THUMBNAILS_EXT, ORIGINALS_EXT, DEFAULT_THUMBNAIL_PATH);

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

$headers = GetAllHeaders();
//Allow cross-domian requests and request methods
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');

//get request parameters
$method = $_SERVER['REQUEST_METHOD'];
$url = substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
list($url, $params) = explode('?', $url, 2);
list($script, $action, $id, $additioanals) = explode('/', $url, 4);

//find requested action and target to apply
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    $timestamp = time();
    switch ($action) {
        case 'menu-images':
            $album = new ItemsImagesHandler($imagesOptions, $headers, $_FILES);
            switch ($method) {
                case 'POST':
                    if (isset($id)) {
                        //save file and update menu item record
                        $res = $album->update($id, $timestamp);
                        $itemsModel->setItemImage($mysqlLink, $id, true, $timestamp);
                    } else {
                        throw new Exception('No Item ID', 15);
                    }
                    break;
                case 'DELETE':
                    //delete file and update menu item record
                    $album->delete($id);
                    $itemsModel->setItemImage($mysqlLink, $id, false, null);
                    break;
            }
            break;
        default: throw new Exception('Unknown action', 15);
    }
    
    $databaseAccess->dbClose($mysqlLink);
    
    if (isset($res)) {
        echo json_encode($res);
    }
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    errorResponse();
    if ($mysqlLink != null) {
        echo json_encode(array('error' => array('msg' => $e->getMessage(), 'code' => $e->getCode())));
    } else {
        echo json_encode(array('error' => array('msg' => 'Cannot connect to Database', 'code' => $e->getCode())));
    }
    echo json_encode($errorData); 
}
