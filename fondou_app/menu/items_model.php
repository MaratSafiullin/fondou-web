<?php

/**
 * Class to implement menu items data model. Administrator's point of view and website displaying 
 */
class ItemsModel {

    //parameters related to item images in file system
    private $thumbnailsPath;
    private $imagesPath;
    private $thumbnailsExtention;
    private $imagesExtention;
    private $defaultThumbnail;

    /**
     * Class constructor to set item images parameters
     * @param $thumbnailsPath
     * @param $imagesPath
     * @param $thumbnailsExtention
     * @param $imagesExtention
     * @param $defaultThumbnail
     */
    public function __construct($thumbnailsPath, $imagesPath, $thumbnailsExtention, $imagesExtention, $defaultThumbnail) {

        $this->thumbnailsPath = $thumbnailsPath;
        $this->imagesPath = $imagesPath;
        $this->thumbnailsExtention = $thumbnailsExtention;
        $this->imagesExtention = $imagesExtention;
        $this->defaultThumbnail = $defaultThumbnail;
    }

    /**
     * Get list of menu items (may select only items that belong to particular menu category)
     * @param $mysqlLink Link to DB connection
     * @param $categoryId Category id, cam be null for all categories
     * @return \ItemRecord Array of menu item records
     */
    function getItemsList($mysqlLink, $categoryId) {
        //Apply category filter if necessary
        if ($categoryId != null) {
            $query = "SELECT id, name, description, has_image, image, thumbnail, active, price, size_id FROM tbl_menu_items WHERE category_id = ? ORDER BY name";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "i", $categoryId);
        } else {
            $query = "SELECT id, name, description, has_image, image, thumbnail, active, price, size_id FROM tbl_menu_items ORDER BY name";
            $stmt = mysqli_prepare($mysqlLink, $query);
        }
        //
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $hasImage, $image, $thumbnail, $active, $price, $sizeId);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $active = (boolean) $active;
            $record = new ItemRecord;
            $record->id = $id;
            $record->name = $name;
            $record->description = $description;
            if ($hasImage != 0) {
                $record->thumbnail = $this->thumbnailsPath . $thumbnail;
                $record->image = $this->imagesPath . $image;
            } else {
                $record->thumbnail = $this->defaultThumbnail;
                $record->image = '';
            }
            $record->active = $active;
            $record->price = $price / 100;
            $record->sizeId = $sizeId;
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Read single menu item record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \ItemRecord Menu item record
     */
    function readItem($mysqlLink, $id) {
        $query = "SELECT id, name, description, has_image, image, thumbnail, active, price, size_id FROM tbl_menu_items WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $hasImage, $image, $thumbnail, $active, $price, $sizeId);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $active = (boolean) $active;
            $record = new ItemRecord;
            $record->id = $id;
            $record->name = $name;
            $record->description = $description;
            if ($hasImage != 0) {
                $record->thumbnail = $this->thumbnailsPath . $thumbnail;
                $record->image = $this->imagesPath . $image;
            } else {
                $record->thumbnail = $this->defaultThumbnail;
                $record->image = '';
            }
            $record->active = $active;
            $record->price = $price / 100;  //price in DB is kept as cents integer value
            $record->sizeId = $sizeId;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new menu item record.
     * Different sizes of the same item are considered as different items
     * @param $mysqlLink Link to DB connection
     * @param $categoryId Item category id
     * @param $name Item name
     * @param $description Item description
     * @param $price Item price
     * @param $sizeId Item size id
     * @return \ItemRecord New menu item record
     */
    function createItem($mysqlLink, $categoryId, $name, $description, $price, $sizeId) {
        $price = (integer) ($price * 100); //price in DB is kept as cents integer value
        $query = "INSERT INTO tbl_menu_items (category_id, name, description, price, size_id) VALUES (?, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "issii", $categoryId, $name, $description, $price, $sizeId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readItem($mysqlLink, $id);
    }

    /**
     * Update menu item record.
     * Different sizes of the same item are considered as different items
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $name Item name
     * @param $description Item description
     * @param $price Item price
     * @param $sizeId Item size id
     * @return \ItemRecord Updated menu item record
     */
    function updateItem($mysqlLink, $id, $name, $description, $price, $sizeId) {
        $price = (integer) ($price * 100); //price in DB is kept as cents integer value
        $query = "UPDATE tbl_menu_items SET name = ?, description = ?, price = ?, size_id = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssiii", $name, $description, $price, $sizeId, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readItem($mysqlLink, $id);
    }

    /**
     * Update menu item record. Set "active" parameter 
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $active True/false
     * @return \ItemRecord Updated menu item record
     */
    function setItemActive($mysqlLink, $id, $active) {
        $active = (integer) $active;
        $query = "UPDATE tbl_menu_items SET active = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ii", $active, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readItem($mysqlLink, $id);
    }

    /**
     * Update menu item record. Set "has image" parameter
     * Called after uploading or deleting image for the item 
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $hasImage True/false
     * @param $timestamp Timestamp of uploaded images
     * @return True if successful 
     */
    function setItemImage($mysqlLink, $id, $hasImage, $timestamp) {
        $hasImage = (integer) $hasImage;
        if ($hasImage) {
            //uploded image(thumbnail) file name should depend on item id and uploading action timestamp
            $image = $id . '_' . $timestamp . '.' . $this->imagesExtention;
            $thumbnail = $id . '_' . $timestamp . '.' . $this->thumbnailsExtention;
            //
            $query = "UPDATE tbl_menu_items SET has_image = ?, image = ?, thumbnail = ? WHERE id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "issi", $hasImage, $image, $thumbnail, $id);
        } else {
            $query = "UPDATE tbl_menu_items SET has_image = ?, image = '', thumbnail = '' WHERE id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "ii", $hasImage, $id);
        }
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

    /**
     * Delete menu item record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteItem($mysqlLink, $id) {
        $query = "DELETE FROM tbl_menu_items WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

    //**************************************************************************
    
    /**
     * Get list of menu items that belong to particular menu category for website displaying
     * Items with matching name are treated as one record with subrecords of sizes with different prices
     * For website menu structure is: category(name)->item(name, description)->size(name, price)
     * @param $mysqlLink Link to DB connection
     * @param $categoryId Category id
     * @return \ItemRecord Array of menu item records
    */ 
    function getItemsListWebsite($mysqlLink, $categoryId) {
        $query = "SELECT itm.id, itm.name, itm.description, itm.price, itm.size_id, sz.name "
                . "FROM tbl_menu_items AS itm "
                . "INNER JOIN tbl_sizes AS sz ON sz.order = itm.size_id "
                . "WHERE category_id = ? AND active = 1 "
                . "ORDER BY itm.name, itm.size_id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $categoryId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $price, $sizeId, $sizeName);
        mysqli_stmt_store_result($stmt);

        $result = array();

        //combine by item name
        $record = new ItemRecordWebsite;
        $currentName = "";
        while (mysqli_stmt_fetch($stmt)) {
            if ($name === $currentName) {
                $size = new ItemSizeRecordWebsite;
                $size->id = $sizeId;
                if ($sizeId == 0) {
                    $size->name = '';
                } else {
                    $size->name = $sizeName;
                }
                $size->price = $price / 100;
                $record->sizes[] = $size;
            } else {
                $record = new ItemRecordWebsite;
                $record->id = $id;
                $record->name = $name;
                $record->description = $description;
                $result[] = $record;

                $size = new ItemSizeRecordWebsite;
                $size->id = $sizeId;
                if ($sizeId == 0) {
                    $size->name = '';
                } else {
                    $size->name = $sizeName;
                }
                $size->price = $price / 100;
                $record->sizes[] = $size;

                $currentName = $name;
            }
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

}

/**
 * Class - menu item record for dashboard
 */
class ItemRecord {

    public $id;
    public $name;
    public $description;
    public $thumbnail = '';
    public $image = '';
    public $active;
    public $price;
    public $sizeId;

}

/**
 * Class - menu item record for website displaying
 */
class ItemRecordWebsite {

    public $id;
    public $name;
    public $description;
    public $sizes = array();

}

/**
 * Class - menu item size for website displaying
 */
class ItemSizeRecordWebsite {

    public $id;
    public $name;
    public $price;

}