<?php

/**
 * Class to implement menu categories data model. Administrator's point of view and website displaying 
 */
class CategoriesModel {

    /**
     * Get list of menu categories
     * @param $mysqlLink Link to DB connection
     * @return \CategoryRecord Array of menu category records
     */
    function getCategoriesList($mysqlLink) {
        $query = "SELECT id, name, ordering_simple, ordering_advanced FROM tbl_menu_categories ORDER BY name";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $orderingSimple, $orderingAdvanced);
        mysqli_stmt_store_result($stmt);
        $result = array();
        while (mysqli_stmt_fetch($stmt)) {
            $orderingSimple = (boolean) $orderingSimple;
            $orderingAdvanced = (boolean) $orderingAdvanced;
            $record = new CategoryRecord;
            $record->id = $id;
            $record->name = $name;
            $record->orderingSimple = $orderingSimple;
            $record->orderingAdvanced = $orderingAdvanced;
            $result[] = $record;
        }
        mysqli_stmt_close($stmt);

        return $result;
    }
    
    /**
     * Read single menu category record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \CategoryRecord Menu category record
     */
    function readCategory($mysqlLink, $id) {
        $query = "SELECT name, ordering_simple, ordering_advanced FROM tbl_menu_categories WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $orderingSimple, $orderingAdvanced);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $orderingSimple = (boolean) $orderingSimple;
            $orderingAdvanced = (boolean) $orderingAdvanced;
            $record = new CategoryRecord;
            $record->id = $id;
            $record->name = $name;
            $record->orderingSimple = $orderingSimple;
            $record->orderingAdvanced = $orderingAdvanced;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new menu category record
     * @param $mysqlLink Link to DB connection
     * @param $name New category name
     * @return \CategoryRecord New category record
     */
    function createCategory($mysqlLink, $name) {
        $query = "INSERT INTO tbl_menu_categories (name) VALUES (?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $name);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readCategory($mysqlLink, $id);
    }

    /**
     * Update menu category record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $name Category name
     * @param $orderingSimple Category item can be used in simple orders
     * @param $orderingAdvanced Category item can be used in advanced orders
     * @return \CategoryRecord Updated category record
     */
    function updateCategory($mysqlLink, $id, $name, $orderingSimple, $orderingAdvanced) {
        $orderingSimple = (integer) $orderingSimple;
        $orderingAdvanced = (integer) $orderingAdvanced;
        $query = "UPDATE tbl_menu_categories SET name = ?, ordering_simple = ?, ordering_advanced = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "siii", $name, $orderingSimple, $orderingAdvanced, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readCategory($mysqlLink, $id);
    }

    /**
     * Check if menu category record can be deleted (category has no items or associated options)
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if can be deleted 
     */
    function checkCategoryDeletable($mysqlLink, $id) {
        $query = "SELECT id FROM tbl_menu_items WHERE category_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRowsItems = $stmt->num_rows;
        mysqli_stmt_close($stmt);
        
        $query = "SELECT id FROM tbl_categories_options WHERE category_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRowsOptions = $stmt->num_rows;
        mysqli_stmt_close($stmt);

        if (($numRowsItems > 0) || ($numRowsOptions > 0)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Delete menu category record. Will throw exception if category has items or associated options
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteCategory($mysqlLink, $id) {
        if (!$this->checkCategoryDeletable($mysqlLink, $id)) {
            throw new Exception('Cannot delete a Category with Items or Options', 15);
        }
        $query = "DELETE FROM tbl_menu_categories WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

    //**************************************************************************
    
    /**
     * Get list of menu categories for website displaying
     * For each category record get list of menu items (uses menu items model)
     * @param $mysqlLink Link to DB connection
     * @return \CategoryRecordWebsite Array of menu category records
     */
    function getMenuListWebsite($mysqlLink) {
        $query = "SELECT id, name FROM tbl_menu_categories "
                . "WHERE ordering_advanced <> 1 "
                . "ORDER BY name";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name);
        mysqli_stmt_store_result($stmt);
        
        $result = array();
        $itemsModel = new ItemsModel(null, null, null, null, null);
        
        while (mysqli_stmt_fetch($stmt)) {
            $record = new CategoryRecordWebsite;
            $record->id = $id;
            $record->name = $name;
            $result[] = $record;
            
            $record->items = $itemsModel->getItemsListWebsite($mysqlLink, $id);
        }
        mysqli_stmt_close($stmt);

        return $result;
    }

}

/**
 * Class - menu category record for dashboard
 */
class CategoryRecord {

    public $id;
    public $name;
    public $orderingSimple;
    public $orderingAdvanced;

}

/**
 * Class - menu category record for website displaying
 */
class CategoryRecordWebsite {

    public $id;
    public $name;
    public $items = array();

}