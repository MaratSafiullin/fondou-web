<?php

/**
 * Class to implement menu data model. Client's point of view
 */
class ApiMenuModel {

    /**
     * Get fresh menu info for client. 
     * All related record that have been chanaged since given time.
     * Lists of IDs of all related records to check if somethig has been deleted since given time.
     * @param $mysqlLink Link to DB connection
     * @param $timestamp Time of previous refresh
     * @return \MenuList Menu object (fresh menu info) 
     */
    function getMenu($mysqlLink, $timestamp) {
        $result = new MenuList();
        //timestamp for menu info (time of actualization)
        //----------------------------------------------------------------------
        $query = "SELECT CURRENT_TIMESTAMP";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $newTimestamp);
        mysqli_stmt_store_result($stmt);
        mysqli_stmt_fetch($stmt);
        $result->timestamp = $newTimestamp;
        //list of menu category record IDs
        //----------------------------------------------------------------------
        $query = "SELECT id FROM tbl_menu_categories WHERE ordering_simple = 1 OR ordering_advanced = 1";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->categoryIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //list of menu item record IDs
        //----------------------------------------------------------------------
        $query = "SELECT itm.id FROM tbl_menu_items AS itm "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = itm.category_id "
                . "WHERE cat.ordering_simple = 1 OR cat.ordering_advanced = 1";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->itemIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //list of category to option association record IDs
        //----------------------------------------------------------------------
        $query = "SELECT cop.id FROM tbl_categories_options AS cop "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE cat.ordering_simple = 1 OR cat.ordering_advanced = 1";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->categoryOptionIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //list of menu option record IDs
        //----------------------------------------------------------------------
        $query = "SELECT opt.id FROM tbl_options AS opt "
                . "INNER JOIN tbl_categories_options AS cop ON cop.option_id = opt.id "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE cat.ordering_simple = 1 OR cat.ordering_advanced = 1 "
                . "GROUP BY opt.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->optionIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //list of option value record IDs
        //----------------------------------------------------------------------
        $query = "SELECT val.id FROM tbl_options_values AS val "
                . "INNER JOIN tbl_options AS opt ON opt.id = val.option_id "
                . "INNER JOIN tbl_categories_options AS cop ON cop.option_id = opt.id "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE cat.ordering_simple = 1 OR cat.ordering_advanced = 1 "
                . "GROUP BY val.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->optionValueIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //fresh menu category records
        //----------------------------------------------------------------------
        $query = "SELECT id, name, ordering_simple, ordering_advanced "
                . "FROM tbl_menu_categories "
                . "WHERE timestamp > ? AND (ordering_simple = 1 OR ordering_advanced = 1) "
                . "ORDER BY id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $timestamp);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $orderingSimple, $orderingAdvanced);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $orderingSimple = (boolean) $orderingSimple;
            $orderingAdvanced = (boolean) $orderingAdvanced;
            $record = new CategoryRecord;
            $record->id = $id;
            $record->name = $name;
            $record->orderingSimple = $orderingSimple;
            $record->orderingAdvanced = $orderingAdvanced;
            $result->categories[] = $record;
        }
        mysqli_stmt_close($stmt);
        //fresh menu item records
        //----------------------------------------------------------------------
        $query = "SELECT itm.id, itm.name, itm.description, itm.active, itm.price, itm.category_id, itm.size_id, size.name "
                . "FROM tbl_menu_items AS itm "
                . "INNER JOIN tbl_sizes AS size ON size.order = itm.size_id "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = itm.category_id "
                . "WHERE itm.timestamp > ? AND (cat.ordering_simple = 1 OR cat.ordering_advanced = 1) "
                . "ORDER BY itm.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $timestamp);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $active, $price, $categoryId, $sizeId, $sizeName);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $active = (boolean) $active;
            $record = new ItemRecord;
            $record->id = $id;
            $record->name = $name;
            $record->sizeNum = $sizeId;
            if ($sizeId == 0) {
                $record->size = '';
            } else {
                $record->size = $sizeName;
            }
            $record->description = $description;
            $record->active = $active;
            $record->price = $price;
            $record->categoryId = $categoryId;
            $result->items[] = $record;
        }
        mysqli_stmt_close($stmt);
        //fresh category to option association records
        //----------------------------------------------------------------------
        $query = "SELECT cop.id, cop.category_id, cop.option_id "
                . "FROM tbl_categories_options AS cop "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE cop.timestamp > ? AND (cat.ordering_simple = 1 OR cat.ordering_advanced = 1) "
                . "ORDER BY cop.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $timestamp);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $categoryId, $optionId);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $record = new CategoryOptionRecord;
            $record->id = $id;
            $record->categoryId = $categoryId;
            $record->optionId = $optionId;
            $result->categoryOptions[] = $record;
        }
        mysqli_stmt_close($stmt);
        //fresh menu option records
        //----------------------------------------------------------------------
        $query = "SELECT opt.id, opt.name "
                . "FROM tbl_options AS opt "
                . "INNER JOIN tbl_categories_options AS cop ON cop.option_id = opt.id "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE opt.timestamp > ? AND (cat.ordering_simple = 1 OR cat.ordering_advanced = 1) "
                . "GROUP BY opt.id "
                . "ORDER BY opt.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $timestamp);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $record = new OptionRecord;
            $record->id = $id;
            $record->name = $name;
            $result->options[] = $record;
        }
        mysqli_stmt_close($stmt);
        //fresh option value records
        //----------------------------------------------------------------------
        $query = "SELECT val.id, val.option_id, val.order, val.value, val.price_modifier "
                . "FROM tbl_options_values AS val "
                . "INNER JOIN tbl_options AS opt ON opt.id = val.option_id "
                . "INNER JOIN tbl_categories_options AS cop ON cop.option_id = opt.id "
                . "INNER JOIN tbl_menu_categories AS cat ON cat.id = cop.category_id "
                . "WHERE val.timestamp > ? AND (cat.ordering_simple = 1 OR cat.ordering_advanced = 1) "
                . "GROUP BY val.id "
                . "ORDER BY val.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $timestamp);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $optionId, $order, $value, $priceModifier);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $record = new OptionValueRecord;
            $record->id = $id;
            $record->optionId = $optionId;
            $record->order = $order;
            $record->value = $value;
            $record->priceModifier = $priceModifier;
            $result->optionValues[] = $record;
        }
        mysqli_stmt_close($stmt);
        //----------------------------------------------------------------------
        return $result;
    }

}

/**
 * Class - response object for client app with fresh menu information
 */
class MenuList {

    public $timestamp;
    public $categoryIds = array();
    public $itemIds = array();
    public $optionIds = array();
    public $optionValueIds = array();
    public $categoryOptionIds = array();
    public $categories = array();
    public $items = array();
    public $options = array();
    public $optionValues = array();
    public $categoryOptions = array();

}

/**
 * Class - menu category record for client app
 */
class CategoryRecord {

    public $id;
    public $name;
    public $orderingSimple;
    public $orderingAdvanced;

}

/**
 * Class - menu item record for client app
 */
class ItemRecord {

    public $id;
    public $categoryId;
    public $name;
    public $size;
    public $description;
    public $price;
    public $sizeNum;
    public $active;

}

/**
 * Class - menu option record for client app
 */
class OptionRecord {

    public $id;
    public $name;

}

/**
 * Class - option value record for client app
 */
class OptionValueRecord {

    public $id;
    public $optionId;
    public $order;
    public $value;
    public $priceModifier;

}

/**
 * Class - option to category association record for client app
 */
class CategoryOptionRecord {

    public $id;
    public $categoryId;
    public $optionId;

}