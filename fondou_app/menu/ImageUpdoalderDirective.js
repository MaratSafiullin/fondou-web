//Directive for menu item image uploader
app.directive('imageUploader', ['$http', function ($http) {
        return {
            restrict: 'E',
            //links to menu item object and dialogs from main controller
            scope: {
                menuItem: '=',
                errorMessageDialog: '=',
                confirmationDialog: '='
            },
            //
            templateUrl: '/fondou_app/menu/image_uploader.html',
            link: function (scope, element, attrs) {
                //uploaded file
                scope.file = {};
                //App API to call
                var url = '/fondou_app/menu/image_manager.php/menu-images/';
                //action on file field change (selecting file), upload selected file
                scope.options = {
                    change: function (file) {
                        //update image
                        scope.updateItemImage(file);
                    }
                };

                //save (update) menu item image
                scope.updateItemImage = function (file) {
                    file.$upload(url + scope.menuItem.id, scope.file)
                            .then(function (result) {
                                //show new image on webpage
                                scope.reloadItemImage(scope.menuItem);
                            })
                            .catch(function (err) {
                                console.log(err);
                                dialogParameters = {
                                    header: 'Menu Item Image Error',
                                    message: 'Cannot load image'
                                };
                                scope.errorMessageDialog(dialogParameters);
                            });
                };

                //refresh menu item image on webpage
                scope.reloadItemImage = function (menuItem) {
                    menuItem.thumbnail = '/img/menu_items/thumbnails/default.png';
                    menuItem.image = '/img/menu_items/thumbnails/default.png';
                    $http.post("/fondou_app/menu/menu_manager.php", {
                        action: 'ReadOneItem',
                        item_id: menuItem.id
                    })
                            .success(function (item) {
                                //set new refs for "<img>" teg
                                menuItem.thumbnail = item.thumbnail;
                                menuItem.image = item.image;
                            })
                            .error(function (err) {
                                dialogParameters = {
                                    header: 'Menu Item Error',
                                    message: 'Cannot reload item'
                                };
                                scope.errorMessageDialog(dialogParameters);
                            });
                };

                //delete menu item image
                scope.deleteItemImage = function () {
                    dialogParameters = {
                        header: 'Delete Item Image',
                        message: 'Delete Item Image?',
                        isDoable: true
                    };

                    //show confirmation dialog
                    dialog = scope.confirmationDialog(dialogParameters);

                    //delete if confirmed
                    dialog.result.then(function () {
                        $http.delete(url + scope.menuItem.id, {})
                                .success(function (result) {
                                    //show new image on webpage
                                    scope.reloadItemImage(scope.menuItem);
                                })
                                .error(function (err) {
                                    dialogParameters = {
                                        header: 'Menu Item Image Error',
                                        message: 'Cannot delete image'
                                    };
                                    scope.errorMessageDialog(dialogParameters);
                                });
                    });
                };
            }
        };
    }]);