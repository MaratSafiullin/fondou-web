<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './categories_model.php';
include_once './items_model.php';
include_once './images_options.php';
include_once './items_images_handler.php';
include_once '../settings/settings_model.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$categoriesModel = new CategoriesModel;
$itemsModel = new ItemsModel(THUMBNAILS_PATH, ORIGINALS_PATH, THUMBNAILS_EXT, ORIGINALS_EXT, DEFAULT_THUMBNAIL_PATH);
$settingsModel = new SettingsModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$categoryId = filter_input(INPUT_POST, 'category_id');
$categoryName = filter_input(INPUT_POST, 'category_name');
$orderingSimple = filter_input(INPUT_POST, 'ordering_simple');
$orderingSimple = $orderingSimple === 'true' ? true : false;
$orderingAdvanced = filter_input(INPUT_POST, 'ordering_advanced');
$orderingAdvanced = $orderingAdvanced === 'true' ? true : false;
$itemId = filter_input(INPUT_POST, 'item_id');
$itemName = filter_input(INPUT_POST, 'item_name');
$itemDescription = filter_input(INPUT_POST, 'item_description');
$itemActive = filter_input(INPUT_POST, 'item_active');
$itemActive = $itemActive === 'true' ? true : false;
$itemPrice = filter_input(INPUT_POST, 'item_price');
$sizeId = filter_input(INPUT_POST, 'size_id');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadCategories":
            $categoriesList = $categoriesModel->getCategoriesList($mysqlLink);
            echo json_encode($categoriesList);
            break;
        case "CreateCategory":
            $newCategory = $categoriesModel->createCategory($mysqlLink, $categoryName);
            echo json_encode($newCategory);
            break;
        case "UpdateCategory":
            $category = $categoriesModel->updateCategory($mysqlLink, $categoryId, $categoryName, $orderingSimple, $orderingAdvanced);
            echo json_encode($category);
            break;
        case "CheckCategoryDeletable":
            if ($categoriesModel->checkCategoryDeletable($mysqlLink, $categoryId)) {
                echo "YES";
            } else {
                echo "NO";
            }
            break;
        case "DeleteCategory":
            if ($categoriesModel->deleteCategory($mysqlLink, $categoryId)) {
                echo "SUCCESS";
            }
            break;
        case "ReadItems":
            $itemsList = $itemsModel->getItemsList($mysqlLink, $categoryId);
            echo json_encode($itemsList);
            break;
        case "ReadOneItem":
            $item = $itemsModel->readItem($mysqlLink, $itemId);
            echo json_encode($item);
            break;
        case "CreateItem":
            $newItem = $itemsModel->createItem($mysqlLink, $categoryId, $itemName, $itemDescription, $itemPrice, $sizeId);
            echo json_encode($newItem);
            break;
        case "UpdateItem":
            $item = $itemsModel->updateItem($mysqlLink, $itemId, $itemName, $itemDescription, $itemPrice, $sizeId);
            echo json_encode($item);
            break;
        case "SetItemActive":
            $item = $itemsModel->setItemActive($mysqlLink, $itemId, $itemActive);
            echo json_encode($item);
            break;
        case "DeleteItem":
            if ($itemsModel->deleteItem($mysqlLink, $itemId)) {
                echo "SUCCESS";
            }
            break;
        case "ReadSizes":
            $sizesList = $settingsModel->getSizesList($mysqlLink);
            echo json_encode($sizesList);
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}