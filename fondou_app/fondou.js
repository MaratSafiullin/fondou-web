//create AngularJS aplication for dashboard
var app = angular.module('fondou', ['ui.bootstrap', 'oi.file', 'ngRoute'],
        /**
         * Make JQuery style AJAX requests
         */ 
        function ($httpProvider) {
            // Use x-www-form-urlencoded Content-Type
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

            // Redefine the default transformRequest in $http-service
            $httpProvider.defaults.transformRequest = [function (data)
                {
                    /**
                     * Convert an object into x-www-form-urlencoded string.
                     * @param {Object} obj
                     * @return {String}
                     */
                    var param = function (obj)
                    {
                        var query = '';
                        var name, value, fullSubName, subValue, innerObj, i;

                        for (name in obj)
                        {
                            value = obj[name];

                            if (value instanceof Array)
                            {
                                for (i = 0; i < value.length; ++i)
                                {
                                    subValue = value[i];
                                    fullSubName = name + '[' + i + ']';
                                    innerObj = {};
                                    innerObj[fullSubName] = subValue;
                                    query += param(innerObj) + '&';
                                }
                            }
                            else if (value instanceof Object)
                            {
                                for (subName in value)
                                {
                                    subValue = value[subName];
                                    fullSubName = name + '[' + subName + ']';
                                    innerObj = {};
                                    innerObj[fullSubName] = subValue;
                                    query += param(innerObj) + '&';
                                }
                            }
                            else if (value !== undefined && value !== null)
                            {
                                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                            }
                        }

                        return query.length ? query.substr(0, query.length - 1) : query;
                    };

                    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
                }];
        });

//configure AngularJS aplication
app.config(function ($routeProvider) {
    //set routing to display different dashboard pages
    $routeProvider
            .when('/', {
                redirectTo: '/orders/'
            })
            .when('/orders/', {
                controller: 'OrdersController',
                templateUrl: '/fondou_app/orders/orders_view.html'
            })
            .when('/menu/', {
                controller: 'MenuController',
                templateUrl: '/fondou_app/menu/menu_view.html'
            })
            .when('/options/', {
                controller: 'OptionsController',
                templateUrl: '/fondou_app/options/options_view.html'
            })
            .when('/specials/', {
                controller: 'SpecialsController',
                templateUrl: '/fondou_app/specials/specials_view.html'
            })
            .when('/orders-archive/', {
                controller: 'OrdersArchiveController',
                templateUrl: '/fondou_app/orders/orders_archive_view.html'
            })
            .when('/customers/', {
                controller: 'CustomersController',
                templateUrl: '/fondou_app/customers/customers_view.html'
            })
            .when('/settings/', {
                controller: 'SettingsController',
                templateUrl: '/fondou_app/settings/settings_view.html'
            })
            .otherwise({
                redirectTo: '/orders/'
            });
});