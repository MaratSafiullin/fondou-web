<?php

/**
 * Class to implement customers data model. Administrator's point of view
 */
class CustomersModel {

    /**
     * Get list of all customers
     * @param $mysqlLink Link to DB connection
     * @return \CustomerRecordDashboard Array of customer records 
     */
    function getCustomersList($mysqlLink) {
        $query = "SELECT id, name, phone, email, active FROM tbl_customers ORDER BY id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $phone, $email, $active);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $active = (boolean) $active;
            $record = new CustomerRecordDashboard;
            $record->id = $id;
            $record->name = $name;
            $record->phone = $phone;
            $record->email = $email;
            $record->active = $active;
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);
        
        return $result;
    }

    /**
     * Read single customer record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \CustomerRecordDashboard Customer record
     */
    function readCustomer($mysqlLink, $id) {
        $query = "SELECT name, phone, email, active FROM tbl_customers WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $phone, $email, $active);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $active = (boolean) $active;
            $record = new CustomerRecordDashboard;
            $record->id = $id;
            $record->name = $name;
            $record->phone = $phone;
            $record->email = $email;
            $record->active = $active;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Update customer record. Set "active" parameter 
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $active True/false
     * @return \CustomerRecordDashboard Updated customer record
     */
    function setCustomerActive($mysqlLink, $id, $active) {
        $active = (integer) $active;
        $query = "UPDATE tbl_customers SET active = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ii", $active, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readCustomer($mysqlLink, $id);
    }

    /**
     * Update customer record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param type $name New customer name
     * @param type $phone New customer phone
     * @param type $email New customer email
     * @return \CustomerRecordDashboard Updated customer record
     */
    function updateCustomer($mysqlLink, $id, $name, $phone, $email) {
        if (empty($email)) {$email = null;}
        $query = "UPDATE tbl_customers SET name = ?, phone = ?, email = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "sssi", $name, $phone, $email, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readCustomer($mysqlLink, $id);
    }
    
    /**
     * Update customer's password (when customer can't remember the password)
     * Customer token gets regenerated, it will require to redo log in on all customer's devices  
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param type $password New password
     * @return \CustomerRecordDashboard Updated customer record
     */
    function changePassword($mysqlLink, $id, $password) {
        $token = $this->generateToken(100);
        $query = "UPDATE tbl_customers SET password = MD5(?), access_token = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssi", $password, $token, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readCustomer($mysqlLink, $id);
    }

    /**
     * Check if customer record can be deleted (customer has no orders)
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if can be deleted 
     */
    function checkCustomerDeletable($mysqlLink, $id) {
        $query = "SELECT id FROM tbl_orders WHERE customer_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Delete customer record. Will throw exception if customer has orders
     * GCM ids from customer's devices will be deleted too
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteCustomer($mysqlLink, $id) {
        if (!$this->checkCustomerDeletable($mysqlLink, $id)) {
            throw new Exception('Cannot delete a Customer with Orders', 15);
        }
        
        $query = "DELETE FROM tbl_gcm_ids WHERE customer_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
        $query = "DELETE FROM tbl_customers WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

    /**
     * Generate random string to use as a token
     * @param $length Token length
     * @param $chrs Set of characters allowed in the token string (string)
     * @return Token string
     */
    function generateToken($length = 10, $chrs = '1234567890qwertyuiopasdfghjklzxcvbnm') {
        for ($i = 0; $i < $length; $i++) {
            $pwd .= $chrs{mt_rand(0, strlen($chrs) - 1)};
        }
        return $pwd;
    }
}

/**
 * Class - customer record
 */
class CustomerRecordDashboard {

    public $id;
    public $name;
    public $phone;
    public $email;
    public $active;

}