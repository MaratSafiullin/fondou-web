<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './customers_model.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$customersModel = new CustomersModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$customerId = filter_input(INPUT_POST, 'customer_id');
$customerName = filter_input(INPUT_POST, 'customer_name');
$customerPhone = filter_input(INPUT_POST, 'customer_phone');
$customerEmail = filter_input(INPUT_POST, 'customer_email');
$customerActive = filter_input(INPUT_POST, 'customer_active');
$customerActive = $customerActive === 'true' ? true : false;
$password = filter_input(INPUT_POST, 'password');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadCustomers":
            $customersList = $customersModel->getCustomersList($mysqlLink);
            echo json_encode($customersList);
            break;
        case "UpdateCustomer":
            $customer = $customersModel->updateCustomer($mysqlLink, $customerId, $customerName, $customerPhone, $customerEmail);
            echo json_encode($customer);
            break;
        case "SetCustomerActive":
            $customer = $customersModel->setCustomerActive($mysqlLink, $customerId, $customerActive);
            echo json_encode($customer);
            break;
        case "ChangePassword":
            $customer = $customersModel->changePassword($mysqlLink, $customerId, $password);
            echo json_encode($customer);
            break;
        case "CheckCustomerDeletable":
            if ($customersModel->checkCustomerDeletable($mysqlLink, $customerId)) {
                echo "YES";
            } else {
                echo "NO";
            }
            break;
        case "DeleteCustomer":
            if ($customersModel->deleteCustomer($mysqlLink, $customerId)) {
                echo "SUCCESS";
            }
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}