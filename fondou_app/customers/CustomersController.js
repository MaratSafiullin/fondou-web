//Controller for customers page
app.controller('CustomersController', ['$scope', '$http', function ($scope, $http) {
        //list of customers
        $scope.customers = [];

        //***********************************************************************

        //add password property to customer
        $scope.addPasswordField = function (customer) {
            customer.password = '';
        };

        //load customers
        $scope.loadCustomers = function () {
            $http.post("/fondou_app/customers/customers_manager.php", {
                action: 'ReadCustomers'
            })
                    .success(function (customers) {
                        //add password field to every customer
                        for (i = 0; i < customers.length; i++) {
                            $scope.addPasswordField(customers[i]);
                        }
                        //
                        $scope.customers = customers;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Customer Error',
                            message: 'Cannot load list of customers'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update customer, show result
        $scope.updateCustomer = function (index) {
            dialogParameters = {
                header: 'Update Customer',
                message: 'Sure to update customer`s info?',
                isDoable: true
            };

            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);

            //update if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/customers/customers_manager.php", {
                    action: 'UpdateCustomer',
                    customer_id: $scope.customers[index].id,
                    customer_name: $scope.customers[index].name,
                    customer_phone: $scope.customers[index].phone,
                    customer_email: $scope.customers[index].email
                })
                        .success(function (customer) {
                            //add password field to customer
                            $scope.addPasswordField(customer);
                            //
                            $scope.customers[index] = customer;
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Customer Error',
                                message: 'Cannot update customer'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //update customer - set "active" property, show result
        $scope.setCustomerActive = function (index) {
            $http.post("/fondou_app/customers/customers_manager.php", {
                action: 'SetCustomerActive',
                customer_id: $scope.customers[index].id,
                customer_active: $scope.customers[index].active
            })
                    .success(function (customer) {
                        //add password field to customer
                        $scope.addPasswordField(customer);
                        //
                        $scope.customers[index] = customer;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Customer Error',
                            message: 'Cannot change customer`s "active" status'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update customer - set new password, show result
        $scope.changeCustomerPassword = function (index) {
            dialogParameters = {
                header: 'Change Customer`s password',
                message: 'Sure to change customer`s password?',
                isDoable: true
            };
            
            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);
            
            //update if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/customers/customers_manager.php", {
                    action: 'ChangePassword',
                    customer_id: $scope.customers[index].id,
                    password: $scope.customers[index].password
                })
                        .success(function (customer) {
                            //add password field to customer
                            $scope.addPasswordField(customer);
                            //
                            $scope.customers[index] = customer;
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Customer Error',
                                message: 'Cannot change password'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };
        
        //delete customer if possible, remove from customers list
        $scope.deleteCustomer = function (index) {
            //check if can be deleted
            $http.post("/fondou_app/customers/customers_manager.php", {
                action: 'CheckCustomerDeletable',
                customer_id: $scope.customers[index].id
            })
                    .success(function (result) {
                        //if can be deleted ask for confirmation
                        if (result === "YES") {
                            dialogParameters = {
                                header: 'Delete Customer',
                                message: 'Delete Customer?',
                                isDoable: true
                            };
                        } 
                        //if not - notify user
                        else {
                            dialogParameters = {
                                header: 'Delete Customer',
                                message: 'Customer has Orders. Cannot be deleted',
                                isDoable: false
                            };
                        }
                        
                        //show confirmation dialog
                        dialog = $scope.confirmationDialog(dialogParameters);

                        //delete if confirmed
                        dialog.result.then(function () {
                            $http.post("/fondou_app/customers/customers_manager.php", {
                                action: 'DeleteCustomer',
                                customer_id: $scope.customers[index].id
                            })
                                    .success(function (result) {
                                        if (result === "SUCCESS") {
                                            $scope.customers.splice(index, 1);
                                        }
                                    })
                                    .error(function (err) {
                                        dialogParameters = {
                                            header: 'Customer Error',
                                            message: 'Cannot delete a Customer'
                                        };
                                        $scope.errorMessageDialog(dialogParameters);
                                    });
                        });
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Customer Error',
                            message: 'Cannot check if a Customer can be deleted'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //***********************************************************************

        //initialize page
        $scope.loadCustomers();

    }]);