<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once '../notifications/emails.php';
include_once '../settings/api_settings_model.php';
include_once '../settings/settings_model.php';

//Allow cross-domian requests and request methods
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');

//get request parameters
$method = $_SERVER['REQUEST_METHOD'];
$urlFull = substr($_SERVER['REQUEST_URI'], 1);
list($url, $params) = explode('?', $urlFull, 2);
list($prefix, $action, $id, $additioanals) = explode('/', $url, 4);
$_PUT = array();
$_DELETE = array();
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $putdata = file_get_contents('php://input');
    $exploded = explode('&', $putdata);

    foreach ($exploded as $pair) {
        $item = explode('=', $pair);
        if (count($item) == 2) {
            $_PUT[urldecode($item[0])] = urldecode($item[1]);
        }
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = $_GET;
//    $deletedata = file_get_contents('php://input');
//    $exploded = explode('&', $deletedata);
//
//    foreach ($exploded as $pair) {
//        $item = explode('=', $pair);
//        if (count($item) == 2) {
//            $_DELETE[urldecode($item[0])] = urldecode($item[1]);
//        }
//    }
}

//objects for DB connection and credentials check, used for all actions 
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;

//find requested action and target to apply
//for some actions successful authorization requiered otherwise throw exception
try {
    //assume successful result
    successfulResponse();
    //
    $mysqlLink = $databaseAccess->dbOpen();

    switch ($action) {
        //customer (account) actions
        case 'customers':
            switch ($method) {
                //register new account    
                case 'POST':
                    $phone = filter_input(INPUT_POST, 'phone');
                    $name = filter_input(INPUT_POST, 'name');
                    $email = filter_input(INPUT_POST, 'email');
                    $password = filter_input(INPUT_POST, 'password');
                    $customer = $authorization->createCustomer($mysqlLink, $phone, $name, $email, $password);
                    echo json_encode($customer);
                    //send email notification
                    try {
                        $emailSender = new EmailSender;
                        $emailSender->newCustomerEmail($mysqlLink, $customer);
                    } catch (Exception $e) {
                        //ignore
                    }
                    break;
                case 'GET':
                    switch ($additioanals) {
                        //check if phone and email are free (not registered)
                        case 'phone-email-check':
                            $phone = filter_input(INPUT_GET, 'phone');
                            $email = filter_input(INPUT_GET, 'email');
                            $result = $authorization->isPhoneEmailExists($mysqlLink, $phone, $email);
                            echo json_encode($result);
                            break;
                        //try to log in
                        case 'login':
                            $phone = filter_input(INPUT_GET, 'phone');
                            $password = filter_input(INPUT_GET, 'password');
                            $result = $authorization->loginCustomer($mysqlLink, $phone, $password);
                            echo json_encode($result);
                            break;
                        //check token validity
                        case 'token':
                            $id = filter_input(INPUT_GET, 'id');
                            $token = filter_input(INPUT_GET, 'token');
                            $result = $authorization->checkCustomerToken($mysqlLink, $id, $token);
                            echo json_encode($result);
                            break;
                    }
                    break;
                case 'PUT':
                    switch ($additioanals) {
                        //update customer info
                        case 'info':
                            $name = $_PUT['name'];
                            $email = $_PUT['email'];
                            $customerToken = $_PUT['customer_token'];
                            if ($authorization->isValidToken($mysqlLink, $id, $customerToken)) {
                                $customer = $authorization->updateCustomer($mysqlLink, $id, $name, $email);
                                echo json_encode($customer);
                            } else {
                                throw new Exception('Authorizaton failed');
                            }
                            break;
                        //update customer password
                        case 'password':
                            $oldPassword = $_PUT['old_password'];
                            $newPassword = $_PUT['new_password'];
                            $result = $authorization->updateCustomerPassword($mysqlLink, $id, $oldPassword, $newPassword);
                            echo json_encode($result);
                            break;
                        //register (add) GCM ID for a customer
                        case 'gcm-id':
                            $gcmId = $_PUT['gcm_id'];
                            $customerToken = $_PUT['customer_token'];
                            if ($authorization->isValidToken($mysqlLink, $id, $customerToken)) {
                                $result = $authorization->addGcmId($mysqlLink, $id, $gcmId);
                                echo json_encode($result);
                            } else {
                                throw new Exception('Authorizaton failed');
                            }
                            break;
                        //clear (delete) GCM ID for a customer
                        case 'gcm-id-clear':
                            $gcmId = $_PUT['gcm_id'];
                            $customerToken = $_PUT['customer_token'];
                            if ($authorization->isValidToken($mysqlLink, $id, $customerToken)) {
                                $result = $authorization->deleteGcmId($mysqlLink, $id, $gcmId);
                                echo json_encode($result);
                            } else {
                                throw new Exception('Authorizaton failed');
                            }
                            break;
                    }
                    break;
            }
            break;
        //menu actions
        case 'menu':
            include_once '../menu/api_menu_model.php';
            $apiMenuModel = new ApiMenuModel();
            switch ($method) {
                //get fresh menu info
                case 'GET':
                    $timestamp = filter_input(INPUT_GET, 'timestamp');
                    $menuList = $apiMenuModel->getMenu($mysqlLink, $timestamp);
                    echo json_encode($menuList);
                    break;
            }
            break;
        //orders actions
        case 'orders':
            include_once '../orders/api_orders_model.php';
            include_once '../orders/orders_model.php';
            $apiOrdersModel = new ApiOrdersModel;
            switch ($method) {
                //post new order
                case 'POST':
                    $customerId = filter_input(INPUT_POST, 'customer_id');
                    $customerToken = filter_input(INPUT_POST, 'customer_token');
                    $order = filter_input(INPUT_POST, 'order');
                    if ($authorization->isValidToken($mysqlLink, $customerId, $customerToken)) {
                        $order = $apiOrdersModel->createOrder($mysqlLink, $customerId, $order);
                        echo json_encode($order);
                        //send email notifications
                        try {
                            $authorization = new Authorization;
                            $customer = $authorization->readCustomer($mysqlLink, $customerId);
                            $emailSender = new EmailSender;
                            $emailSender->newOrdersEmail($mysqlLink, $customer, $order);
                        } catch (Exception $e) {
                            //ignore
                        }
                    } else {
                        throw new Exception('Authorizaton failed');
                    }
                    break;
                //get fresh customer's orders info
                case 'GET':
                    $customerId = filter_input(INPUT_GET, 'customer_id');
                    $customerToken = filter_input(INPUT_GET, 'customer_token');
                    $timestamp = filter_input(INPUT_GET, 'timestamp');
                    if ($authorization->isValidToken($mysqlLink, $customerId, $customerToken)) {
                        $ordersList = $apiOrdersModel->getCustomerOrdersList($mysqlLink, $customerId, $timestamp);
                        echo json_encode($ordersList);
                    } else {
                        throw new Exception('Authorizaton failed');
                    }
                    break;
                case 'PUT':
                    switch ($additioanals) {
                        //cancel order
                        case 'cancel' :
                            $customerId = $_PUT['customer_id'];
                            $customerToken = $_PUT['customer_token'];
                            if ($authorization->isValidToken($mysqlLink, $customerId, $customerToken)) {
                                $result = $apiOrdersModel->cancelOrder($mysqlLink, $id, $customerId);
                                echo json_encode($result);
                            } else {
                                throw new Exception('Authorizaton failed');
                            }
                            break;
                    }
                    break;
            }
            break;
        //unknown action
        default: throw new Exception('Unknown action');
    }

    $databaseAccess->dbClose($mysqlLink);
}
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();
    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}