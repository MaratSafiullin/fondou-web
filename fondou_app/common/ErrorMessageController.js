//Controller for error dialog
app.controller('ErrorMessageController', [
    '$scope', '$uibModalInstance', 'parameters', function ($scope, $uibModalInstance, parameters) {
        //dialog parameters
        $scope.header = parameters.header;
        $scope.message = parameters.message;

        //OK button click
        $scope.ok = function () {
            $uibModalInstance.close('ok');
        };
    }]);