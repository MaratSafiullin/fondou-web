<?php

/**
 * Class - exception description
 */
class ErrorData {
    public $description;
    public $stacktrace;
}

/**
 * Success response HTTP header
 */
function successfulResponse() {
    header("Content-Type: text/html; charset=utf-8");
}

/**
 * Error response HTTP header
 */
function errorResponse() {
    header("Content-Type: text/html; charset=utf-8", true, 500);
}

/**
 * Treat all Warnings as Errors
 */
set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if ($errno === 2) {
        throw new ErrorException($errstr, 15, $errno, $errfile, $errline);
    } else {
        return false;
    }
});