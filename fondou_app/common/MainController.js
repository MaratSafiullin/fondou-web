//Controller for the app in general, common functions
app.controller('MainController', ['$scope', '$uibModal', '$http', function ($scope, $uibModal, $http) {

        //Show confirmation dialog
        $scope.confirmationDialog = function (dialogParameters) {
            var modalInstance = $uibModal.open({
                templateUrl: '/fondou_app/common/confirmation.html',
                controller: 'ConfirmationController',
                size: 'sm',
                resolve: {
                    parameters: function () {
                        return dialogParameters;
                    }
                }
            });
            return modalInstance;
        };

       //Show error dialog
       $scope.errorMessageDialog = function (dialogParameters) {
            var modalInstance = $uibModal.open({
                templateUrl: '/fondou_app/common/error_message.html',
                controller: 'ErrorMessageController',
                size: 'sm',
                resolve: {
                    parameters: function () {
                        return dialogParameters;
                    }
                }
            });
            return modalInstance;
        };
    }]);