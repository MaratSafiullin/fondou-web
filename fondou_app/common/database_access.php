<?php

/**
 * Class - database connection hanlder
 */
class DatabaseAccess {

    /**
     * Open connection
     * @return Link to DB connection
     */
    function dbOpen() {
        $mysqlLink = mysqli_connect("localhost", "root", "", "fondou");
        mysqli_query($mysqlLink, "SET NAMES 'utf8'");
        mysqli_query($mysqlLink, "SET CHARACTER SET 'utf8';");
        mysqli_query($mysqlLink, "SET SESSION collation_connection = 'utf8_bin';");
        return $mysqlLink;
    }

    /**
     * Close connection
     * @param $mysqlLink Link to DB connection
     */
    function dbClose($mysqlLink) {
        mysqli_close($mysqlLink);
    }
    
}