//Controller for confirmation dialog
app.controller('ConfirmationController', [
    '$scope', '$uibModalInstance', 'parameters', function ($scope, $uibModalInstance, parameters) {
        //dialog parameters
        $scope.header = parameters.header;
        $scope.message = parameters.message;
        $scope.isDoable = parameters.isDoable;

        //OK button click
        $scope.ok = function () {
            $uibModalInstance.close('ok');
        };

        //Cancel button click
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);