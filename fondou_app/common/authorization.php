<?php

/**
 * Class to implement autoriaztion functions. Administrator autorization, customer autorization, GCM IDs handling
 */
class Authorization {

    //Admin functions
    
    /**
     * Check if there is a session with admin data. Set global variables for nesting script
     * @global $adminID
     * @global $adminLogin
     * @return True if session started
     */
    function checkAdminSession() {
        global $adminID;
        global $adminLogin;
        session_start();
        if (isset($_SESSION['id'])) {
            $adminID = $_SESSION['id'];
            $adminLogin = $_SESSION['login'];
        }
        //if no admin id than return false
        if (empty($adminID)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check admin autorization info saved in cookie if there is any
     * @param $mysqlLink Link to DB connection
     * @return Admin info data object if autorization check successful, null if not or there is no cookie
     */
    function checkCookie($mysqlLink) {
        $user_data_str = filter_input(INPUT_COOKIE, 'user_data');
        if ($user_data_str != null) {
            $user_data = json_decode($user_data_str);

            $query = "SELECT id, login, password FROM tbl_admin WHERE login = ? AND password = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "ss", $user_data->login, $user_data->pass_hash);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id, $name, $pass_hash);
            mysqli_stmt_store_result($stmt);

            if ($stmt->num_rows > 0) {
                return $user_data;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Check admin login/password pair
     * @param $mysqlLink Link to DB connection
     * @param $login Admin login 
     * @param $password Admin password
     * @return \LoginInfo Admin info data object if autorization check successful, null if not
     */
    function checkLoginPassword($mysqlLink, $login, $password) {
        $query = "SELECT id, login, password FROM tbl_admin WHERE login = ? AND password = MD5(?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ss", $login, $password);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $pass_hash);
        mysqli_stmt_store_result($stmt);

        if ($stmt->num_rows > 0) {
            $result = new LoginInfo;
            mysqli_stmt_fetch($stmt);
            $result->id = $id;
            $result->login = $name;
            $result->pass_hash = $pass_hash;
            return $result;
        } else {
            return null;
        }
    }
    
    /**
     * Update admin password.
     * CURRENTLY THERE IS ONLY ONE ADMIN IN THE SYSTEM. WILL BE CHANAGED IF MULTIPLE ADMINS FEATURE IS TO BE IMPLEMENTED 
     * @param $mysqlLink Link to DB connection
     * @param $oldPassword Admin current password
     * @param $newPassword New password
     * @return True if successful
     */
    function changeAdminPassword($mysqlLink, $oldPassword, $newPassword) {
        $query = "SELECT id FROM tbl_admin WHERE login = 'admin' AND password = MD5(?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s",  $oldPassword);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $query = "UPDATE tbl_admin SET password = MD5(?) WHERE login = 'admin'";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "s", $newPassword);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            return true;
        } else {
            return false;
        }
    }

    //**************************************************************************
    
    //Customer functions
    
    /**
     * Generate random string to use as a token
     * @param $length Token length
     * @param $chrs Set of characters allowed in the token string (string)
     * @return Token string
     */
    function generateToken($length = 10, $chrs = '1234567890qwertyuiopasdfghjklzxcvbnm') {
        for ($i = 0; $i < $length; $i++) {
            $pwd .= $chrs{mt_rand(0, strlen($chrs) - 1)};
        }
        return $pwd;
    }

    /**
     * Read single customer record. If successful include app settings in response
     * @param $mysqlLink Link to DB connection
     * @param $id Customer Id
     * @return \CustomerRecord Customer record
     */
    function readCustomer($mysqlLink, $id) {
        $query = "SELECT id, phone, name, email, active, access_token FROM tbl_customers WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $phone, $name, $email, $active, $access_token);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $active = (boolean) $active;
            if (empty($email) || email == 'null') {
                $email = '';
            }
            $record = new CustomerRecord;
            $record->id = $id;
            $record->phone = $phone;
            $record->name = $name;
            $record->email = $email;
            $record->active = $active;
            $record->accessToken = $access_token;
            //add app settings
            $settingsModel = new ApiSettingsModel;
            $record->settings = $settingsModel->getSettings($mysqlLink);
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Check if phone or email is free (not registered)
     * @param $mysqlLink Link to DB connection
     * @param $phone Customer's phone num
     * @param $email Customer's email
     * @return \Response Response object - result text
     */
    function isPhoneEmailExists($mysqlLink, $phone, $email) {
        if (empty($email)) {
            $email = null;
        }
        if ($email == null) {
            $query = "SELECT id FROM tbl_customers WHERE phone = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "s", $phone);
        } else {
            $query = "SELECT id FROM tbl_customers WHERE phone = ? OR email = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "ss", $phone, $email);
        }
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_close($stmt);

        $result = new Response();
        if ($numRows > 0) {
            $result->result = "YES";
        } else {
            $result->result = "NO";
        }
        return $result;
    }

    /**
     * Create new customer record.
     * Throw exception if unsuccessful
     * @param $mysqlLink Link to DB connection
     * @param $phone Customer's phone num
     * @param $name Customer's name
     * @param $email Customer's email
     * @param $password Customer's password
     * @return \CustomerRecord Customer record
     */
    function createCustomer($mysqlLink, $phone, $name, $email, $password) {
        if ((empty($phone)) || empty($name)) {
            throw new Exception('Empty phone or name');
        }
        if (empty($email)) {
            $email = null;
        }
        $token = $this->generateToken(100);
        $query = "INSERT INTO tbl_customers (phone, name, email, password, access_token) VALUES (?, ?, ?, MD5(?), ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "sssss", $phone, $name, $email, $password, $token);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        $customerRecord = $this->readCustomer($mysqlLink, $id);
        if ($customerRecord != null) {
            $customerRecord->result = "SUCCESS";
            return $customerRecord;
        } else {
            throw new Exception('Could not create customer');
        }
    }

    /**
     * Update customer record.
     * Throw exception if customer id is invalid
     * @param $mysqlLink Link to DB connection
     * @param $id Customer Id
     * @param $name Customer's name
     * @param $email Customer's email
     * @return \CustomerRecord Customer record
     */
    function updateCustomer($mysqlLink, $id, $name, $email) {
        if (empty($name)) {
            throw new Exception('Empty name');
        }
        if (empty($email)) {
            $email = null;
        }
        $query = "UPDATE tbl_customers SET name = ?, email = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssi", $name, $email, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $customerRecord = $this->readCustomer($mysqlLink, $id);
        if ($customerRecord != null) {
            $customerRecord->result = "SUCCESS";
            return $customerRecord;
        } else {
            throw new Exception('Could not update customer');
        }
    }

    /**
     * Update customer password.
     * Throw exception if unsuccessful
     * @param $mysqlLink Link to DB connection
     * @param $id Customer Id
     * @param $oldPassword Current password
     * @param $newPassword New Password
     * @return \Response Response object - result text
     */
    function updateCustomerPassword($mysqlLink, $id, $oldPassword, $newPassword) {
        $query = "SELECT id FROM tbl_customers WHERE id = ? AND password = MD5(?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $id, $oldPassword);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        $result = new Response();
        if ($numRows > 0) {
            $query = "UPDATE tbl_customers SET password = MD5(?) WHERE id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "si", $newPassword, $id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            $result->result = "SUCCESS";
        } else {
            $result->result = "FAIL";
        }
        return $result;
    }

    /**
     * Log in customer
     * @param $mysqlLink Link to DB connection
     * @param $phone Customer's phone num
     * @param $password Customer's password
     * @return \CustomerRecord Customer record, may contain only "fail" response if unsuccessful
     */
    function loginCustomer($mysqlLink, $phone, $password) {
        $query = "SELECT id, phone, name, email, active, access_token FROM tbl_customers "
                . "WHERE phone = ? AND password = MD5(?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ss", $phone, $password);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $phone, $name, $email, $active, $access_token);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        $record = new CustomerRecord;
        if ($numRows > 0) {
            if (empty($email) || email == 'null') {
                $email = '';
            }
            $active = (boolean) $active;
            $record->result = "SUCCESS";
            $record->id = $id;
            $record->phone = $phone;
            $record->name = $name;
            $record->email = $email;
            $record->active = $active;
            $record->accessToken = $access_token;
            $settingsModel = new ApiSettingsModel;
            $record->settings = $settingsModel->getSettings($mysqlLink);
        } else {
            $record->result = "FAIL";
        }
        return $record;
    }

    /**
     * Check customer's token and status: valid token, invalid token, inactive customer
     * @param $mysqlLink Link to DB connection
     * @param $id Customer Id
     * @param $token Customer token (string)
     * @return \Response Response object - result text
     */
    function checkCustomerToken($mysqlLink, $id, $token) {
        $query = "SELECT active FROM tbl_customers WHERE id = ? AND access_token = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $id, $token);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $active);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        $result = new Response();
        if ($numRows > 0) {
            $active = (boolean) $active;
            if ($active) {
                $result->result = "OK";
            } else {
                $result->result = "INACTIVE";
            }
            $settingsModel = new ApiSettingsModel;
            $result->settings = $settingsModel->getSettings($mysqlLink);
        } else {
            $result->result = "FAIL";
        }
        return $result;
    }

    /**
     * Check customer's token/validity: if token is valid and customer is active.
     * Used to check if client app can perform actions
     * @param $mysqlLink Link to DB connection
     * @param $id Customer Id
     * @param $token Customer token (string)
     * @return True if valid false otherwise
     */
    function isValidToken($mysqlLink, $id, $token) {
        $query = "SELECT active FROM tbl_customers WHERE id = ? AND access_token = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $id, $token);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $active);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $active = (boolean) $active;
            if ($active) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //**************************************************************************

    /**
     * Register GCM ID for a customer
     * If GCM ID is already in DB it will be re-registered for a given customer anyway
     * @param $mysqlLink Link to DB connection
     * @param $customerId Id of the customer
     * @param $gcmId GCM ID value (string)
     * @return \Response Response object for client app (action result text)
     */
    function addGcmId($mysqlLink, $customerId, $gcmId) {
        //delete GCM ID from DB if belongs to another customer (can happen if change customer account on a device)
        $query = "DELETE FROM tbl_gcm_ids WHERE customer_id <> ? AND gcm_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $customerId, $gcmId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        //check if GCM ID is already registered for a customer
        $query = "SELECT id FROM tbl_gcm_ids WHERE customer_id = ? AND gcm_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $customerId, $gcmId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        //add record if necessary
        if ($numRows === 0) {
            $query = "INSERT INTO tbl_gcm_ids (customer_id, gcm_id) VALUES (?, ?)";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "is", $customerId, $gcmId);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
        }
        
        $response = new Response();
        $response->result = "GCM ID ADDED";
        return $response;
    }

    /**
     * Delete particular GCM ID for a customer.
     * Happens when customer logs out or GCM ID is no longer valid
     * @param $mysqlLink Link to DB connection
     * @param $customerId Id of the customer
     * @param $gcmId GCM ID value (string)
     * @return \Response Response object for client app (action result text)
     */
    function deleteGcmId($mysqlLink, $customerId, $gcmId) {
        $query = "DELETE FROM tbl_gcm_ids WHERE customer_id = ? AND gcm_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $customerId, $gcmId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
        $response = new Response();
        $response->result = "GCM ID DELETED";
        return $response;
    }
}

/**
 * Class - administrator login attempt response
 */
class LoginInfo {

    public $id;
    public $login;
    public $pass_hash;

}

/**
 * Class - customer record
 */
class CustomerRecord {

    public $result;
    public $id;
    public $phone;
    public $name;
    public $email;
    public $active;
    public $accessToken;
    public $settings;

}

/**
 * Class - response. Different actions result
 */
class Response {

    public $result;
    public $settings;

}