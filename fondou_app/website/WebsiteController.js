//Controller for website pages (displayed to customers)
app.controller('WebsiteController', ['$scope', '$http', function ($scope, $http) {
        //list of special offers
        $scope.specials = [];
        //menu list (categories with items)
        $scope.menu = [];
        
        //***********************************************************************

        //load list of special offers
        $scope.loadSpecials = function () {
            $http.post("/fondou_app/website/website_manager.php", {
                action: 'ReadSpecials'
            })
                    .success(function (specials) {
                        $scope.specials = specials;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Error',
                            message: 'Cannot load list of special offers'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };
        
        //load menu (display version)
        $scope.loadMenu = function () {
            $http.post("/fondou_app/website/website_manager.php", {
                action: 'ReadMenu'
            })
                    .success(function (menu) {
                        $scope.menu = menu;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Error',
                            message: 'Cannot load menu'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };
        
        
    }]);