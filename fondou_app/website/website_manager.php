<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../specials/specials_model.php';
include_once '../menu/categories_model.php';
include_once '../menu/items_model.php';

//prepare to work with data model
$databaseAccess = new DatabaseAccess;
$specialsModel = new SpecialsModel;
$categoriesModel = new CategoriesModel;

//get request parameters
$action = filter_input(INPUT_POST, 'action');

//find action, return result
try {
    successfulResponse();
    
    $mysqlLink = $databaseAccess->dbOpen();
    switch ($action) {
        case "ReadSpecials":
            $specialsList = $specialsModel->getSpecialsList($mysqlLink);
            echo json_encode($specialsList);
            break;
        case "ReadMenu":
            $menuList = $categoriesModel->getMenuListWebsite($mysqlLink);
            echo json_encode($menuList);
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}