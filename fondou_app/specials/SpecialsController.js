//Controller for special offers page
app.controller('SpecialsController', ['$scope', '$http', function ($scope, $http) {
        //special offers list
        $scope.specials = [];
        //all menu items list
        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
        $scope.menuItems = [];
        
        //***********************************************************************

        //load special offers
        $scope.loadSpecials = function () {
            $http.post("/fondou_app/specials/specials_manager.php", {
                action: 'ReadSpecials'
            })
                    .success(function (specials) {
                        $scope.specials = specials;
                        //add item-replacement related fields to every special offer
                        //find matching item object for every special offer (if it replaces item) 
                        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
                        for (i = 0; i < specials.length; i++) {
                            $scope.addFieldsToSpecial(specials[i]);
                            $scope.findMenuItemForSpecial(specials[i]);
                        }
                        //
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Special Error',
                            message: 'Cannot load list of specials'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //add item-replacement related fields to every special offer
        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
        $scope.addFieldsToSpecial = function (special) {
            if (special.insteadMenuItem !== null) {
                special.replaceItem = true;
            } else {
                special.replaceItem = false;
            }
            special.menuItem = null;
        };

        //create new special offer, add to special offers list
        $scope.createSpecial = function () {
            $http.post("/fondou_app/specials/specials_manager.php", {
                action: 'CreateSpecial',
                special_name: '',
                special_description: '',
                special_price: 0,
                special_count: 0,
                instead_item_id: null
            })
                    .success(function (newSpecial) {
                        //add item-replacement related fields to special offer
                        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
                        $scope.addFieldsToSpecial(newSpecial);
                        //
                        $scope.specials.push(newSpecial);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Special Error',
                            message: 'Cannot create new special'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update special offer, show result
        $scope.updateSpecial = function (index) {
            //set item-to-replace property
            //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
            var insteadMenuItem;
            if (($scope.specials[index].replaceItem) && ($scope.specials[index].menuItem !== null)) {
                insteadMenuItem = $scope.specials[index].menuItem.id;
            } else {
                insteadMenuItem = null;
            }
            //
            $http.post("/fondou_app/specials/specials_manager.php", {
                action: 'UpdateSpecial',
                special_id: $scope.specials[index].id,
                special_name: $scope.specials[index].name,
                special_description: $scope.specials[index].description,
                special_price: $scope.specials[index].price,
                special_count: $scope.specials[index].count,
                instead_item_id: insteadMenuItem
            })
                    .success(function (special) {
                        //add item-replacement related fields to special offer
                        //find matching item object for special offer (if it replaces item) 
                        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
                        $scope.addFieldsToSpecial(special);
                        $scope.findMenuItemForSpecial(special);
                        //
                        $scope.specials[index] = special;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Special Error',
                            message: 'Cannot save special'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete special offer, remove from special offers list
        $scope.deleteSpecial = function (index) {
            dialogParameters = {
                header: 'Delete Special',
                message: 'Delete Special "' + $scope.specials[index].name + '"?',
                isDoable: true
            };
            
            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);
            
            //delete if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/specials/specials_manager.php", {
                    action: 'DeleteSpecial',
                    special_id: $scope.specials[index].id
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.specials.splice(index, 1);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Special Error',
                                message: 'Cannot delete special'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //load all menu items
        $scope.loadItems = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'ReadItems',
                category_id: null
            })
                    .success(function (menuItems) {
                        $scope.menuItems = menuItems;
                        //find matching item object for every special offer (if it replaces item) 
                        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
                        for (i = 0; i < $scope.specials.length; i++) {
                            $scope.findMenuItemForSpecial($scope.specials[i]);
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Menu Item Error',
                            message: 'Cannot load list of items'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //find matching item object for special offer (if it replaces item) 
        //used to handle item selection "<select>"
        //CURRENTLY NOT USED IN BUSINESS MODEL, NOT SHOWED ON WEBPAGE
        $scope.findMenuItemForSpecial = function (special) {
            for (j = 0; j < $scope.menuItems.length; j++) {
                if ($scope.menuItems[j].id === special.insteadMenuItem) {
                    special.menuItem = $scope.menuItems[j];
                    break;
                }
            }
        };

        //***********************************************************************

        //initialize page
        $scope.loadSpecials();
        $scope.loadItems();
    }]);