<?php

/**
 * Class to implement special offers data model. Administrator's point of view
 */
class SpecialsModel {

     /**
     * Get list of special offers
     * @param $mysqlLink Link to DB connection
     * @return \SpecialRecord Array of special offer records
     */
    function getSpecialsList($mysqlLink) {
        $query = "SELECT id, name, description, price, menu_item_instead_id, count FROM tbl_specials ORDER BY id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $price, $insteadMenuItem, $count);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $record = new SpecialRecord;
            $record->id = $id;
            $record->name = $name;
            $record->description = $description;
            $record->price = $price / 100;  //price in DB is kept as cents integer value
            $record->count = $count;
            $record->insteadMenuItem = $insteadMenuItem;
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Read single special offer record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \SpecialRecord Special offer record
     */
    function readSpecial($mysqlLink, $id) {
        $query = "SELECT id, name, description, price, menu_item_instead_id, count FROM tbl_specials WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $description, $price, $insteadMenuItem, $count);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $record = new SpecialRecord;
            $record->id = $id;
            $record->name = $name;
            $record->description = $description;
            $record->price = $price / 100;  //price in DB is kept as cents integer value
            $record->count = $count;
            $record->insteadMenuItem = $insteadMenuItem;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new special offer record
     * @param $mysqlLink Link to DB connection
     * @param $name Special offer item name
     * @param $description Special offer description
     * @param $price Special offer item price
     * @param $insteadMenuItem Menu item that special offer replaces (NOT USED CURRENTLY)
     * @param $count Special offer items (to offer) count
     * @return \SpecialRecord New special offer record
     */
    function createSpecial($mysqlLink, $name, $description, $price, $insteadMenuItem, $count) {
        $price = (integer) ($price * 100);  //price in DB is kept as cents integer value
        $query = "INSERT INTO tbl_specials (name, description, price, menu_item_instead_id, count) VALUES (?, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssiii", $name, $description, $price, $insteadMenuItem, $count);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readSpecial($mysqlLink, $id);
    }

    /**
     * Update special offer record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $name Special offer item name
     * @param $description Special offer description
     * @param $price Special offer item price
     * @param $insteadMenuItem Menu item that special offer replaces (NOT USED CURRENTLY)
     * @param $count Special offer items (to offer) count
     * @return \SpecialRecord Updated special offer record
     */
    function updateSpecial($mysqlLink, $id, $name, $description, $price, $insteadMenuItem, $count) {
        $price = (integer) ($price * 100);  //price in DB is kept as cents integer value
        $query = "UPDATE tbl_specials SET name = ?, description = ?, price = ?, menu_item_instead_id = ?, count = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssiiii", $name, $description, $price, $insteadMenuItem, $count, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readSpecial($mysqlLink, $id);
    }

    /**
     * Delete special offer record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteSpecial($mysqlLink, $id) {
        $query = "DELETE FROM tbl_specials WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

}

/**
 * Class - special offer record
 */
class SpecialRecord {

    public $id;
    public $name;
    public $description;
    public $price;
    public $count;
    public $insteadMenuItem;

}
