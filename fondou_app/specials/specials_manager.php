<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './specials_model.php';
include_once '../menu/items_model.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$specialsModel = new SpecialsModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$specialId = filter_input(INPUT_POST, 'special_id');
$specialName = filter_input(INPUT_POST, 'special_name');
$specialDescription = filter_input(INPUT_POST, 'special_description');
$specialPrice = filter_input(INPUT_POST, 'special_price');
$specialCount = filter_input(INPUT_POST, 'special_count');
$insteadItemId = filter_input(INPUT_POST, 'instead_item_id');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadSpecials":
            $specialsList = $specialsModel->getSpecialsList($mysqlLink);
            echo json_encode($specialsList);
            break;
        case "CreateSpecial":
            $newSpecial = $specialsModel->createSpecial($mysqlLink, $specialName, $specialDescription, $specialPrice, $insteadItemId, $specialCount);
            echo json_encode($newSpecial);
            break;
        case "UpdateSpecial":
            $special = $specialsModel->updateSpecial($mysqlLink, $specialId, $specialName, $specialDescription, $specialPrice, $insteadItemId, $specialCount);
            echo json_encode($special);
            break;
        case "DeleteSpecial":
            if ($specialsModel->deleteSpecial($mysqlLink, $specialId)) {
                echo "SUCCESS";
            }
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}