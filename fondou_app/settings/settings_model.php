<?php

/**
 * Class to implement app settings data model. Administrator's point of view
 * Also handles item sizes model 
 */
class SettingsModel {

    /**
     * Get list of app settings as an object
     * @param $mysqlLink Link to DB connection
     * @return \Settings Settings object
     */
    function getSettings($mysqlLink) {
        $query = "SELECT name, value FROM tbl_settings";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $value);
        mysqli_stmt_store_result($stmt);

        $settings = new Settings();

        while (mysqli_stmt_fetch($stmt)) {
            switch ($name) {
                case "MinimumTimeFrameSimple":
                    $settings->minimumTimeFrameSimple = (integer) $value;
                    break;
                case "MinimumTimeFrameAdvanced":
                    $settings->minimumTimeFrameAdvanced = (integer) $value;
                    break;
                case "OrderingStartHour":
                    $settings->orderingStartHour = (integer) $value;
                    break;
                case "OrderingStartMinute":
                    $settings->orderingStartMinute = (integer) $value;
                    break;
                case "OrderingFinishHour":
                    $settings->orderingFinishHour = (integer) $value;
                    break;
                case "OrderingFinishMinute":
                    $settings->orderingFinishMinute = (integer) $value;
                    break;
                case "MaxNoDepositCostForSimpleOrder":
                    $settings->maxNoDepositCostForSimpleOrder = (integer) $value;
                    break;
                case "MaxNoDepositCostForAdvancedOrder":
                    $settings->maxNoDepositCostForAdvancedOrder = (integer) $value;
                    break;
                case "EmailsForCustomersNotifications":
                    $settings->emailsForCustomersNotifications = $value;
                    break;
                case "EmailsForOrdersNotifications":
                    $settings->emailsForOrdersNotifications = $value;
                    break;
            }
        }
        mysqli_stmt_close($stmt);

        return $settings;
    }

    /**
     * Update app settings
     * @param $mysqlLink Link to DB connection
     * @param $settings New options object
     * @return Settings Updated options object
     */
    function saveSettings($mysqlLink, $settings) {
        $settings = json_decode($settings);

        $query = "UPDATE tbl_settings SET value = ? WHERE name = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);

        $name = 'MinimumTimeFrameSimple';
        $minimumTimeFrameSimple = (integer) $settings->minimumTimeFrameSimple;
        if ($minimumTimeFrameSimple < 0) {
            $minimumTimeFrameSimple = 0;
        }
        mysqli_stmt_bind_param($stmt, "ss", $minimumTimeFrameSimple, $name);
        mysqli_stmt_execute($stmt);

        $name = 'MinimumTimeFrameAdvanced';
        $minimumTimeFrameAdvanced = (integer) $settings->minimumTimeFrameAdvanced;
        if ($minimumTimeFrameAdvanced < 0) {
            $minimumTimeFrameAdvanced = 0;
        }
        mysqli_stmt_bind_param($stmt, "ss", $minimumTimeFrameAdvanced, $name);
        mysqli_stmt_execute($stmt);

        $name = 'OrderingStartHour';
        $orderingStartHour = (integer) $settings->orderingStartHour;
        if ($orderingStartHour < 0) {
            $orderingStartHour = 0;
        }
        if ($orderingStartHour > 23) {
            $orderingStartHour = 23;
        }
        mysqli_stmt_bind_param($stmt, "ss", $orderingStartHour, $name);
        mysqli_stmt_execute($stmt);

        $name = 'OrderingStartMinute';
        $orderingStartMinute = (integer) $settings->orderingStartMinute;
        if ($orderingStartMinute < 0) {
            $orderingStartMinute = 0;
        }
        if ($orderingStartMinute > 59) {
            $orderingStartMinute = 59;
        }
        mysqli_stmt_bind_param($stmt, "ss", $orderingStartMinute, $name);
        mysqli_stmt_execute($stmt);

        $name = 'OrderingFinishHour';
        if ($orderingFinishHour < 0) {
            $orderingFinishHour = 0;
        }
        if ($orderingFinishHour > 23) {
            $orderingFinishHour = 23;
        }
        $orderingFinishHour = (integer) $settings->orderingFinishHour;
        mysqli_stmt_bind_param($stmt, "ss", $orderingFinishHour, $name);
        mysqli_stmt_execute($stmt);

        $name = 'OrderingFinishMinute';
        $orderingFinishMinute = (integer) $settings->orderingFinishMinute;
        if ($orderingFinishMinute < 0) {
            $orderingFinishMinute = 0;
        }
        if ($orderingFinishMinute > 59) {
            $orderingFinishMinute = 59;
        }
        mysqli_stmt_bind_param($stmt, "ss", $orderingFinishMinute, $name);
        mysqli_stmt_execute($stmt);

        $name = 'MaxNoDepositCostForSimpleOrder';
        $maxNoDepositCostForSimpleOrder = (integer) $settings->maxNoDepositCostForSimpleOrder;
        if ($maxNoDepositCostForSimpleOrder < 0) {
            $maxNoDepositCostForSimpleOrder = 0;
        }
        mysqli_stmt_bind_param($stmt, "ss", $maxNoDepositCostForSimpleOrder, $name);
        mysqli_stmt_execute($stmt);

        $name = 'MaxNoDepositCostForAdvancedOrder';
        $maxNoDepositCostForAdvancedOrder = (integer) $settings->maxNoDepositCostForAdvancedOrder;
        if ($maxNoDepositCostForAdvancedOrder < 0) {
            $maxNoDepositCostForAdvancedOrder = 0;
        }
        mysqli_stmt_bind_param($stmt, "ss", $maxNoDepositCostForAdvancedOrder, $name);
        mysqli_stmt_execute($stmt);

        $name = 'EmailsForCustomersNotifications';
        mysqli_stmt_bind_param($stmt, "ss", $settings->emailsForCustomersNotifications, $name);
        mysqli_stmt_execute($stmt);

        $name = 'EmailsForOrdersNotifications';
        mysqli_stmt_bind_param($stmt, "ss", $settings->emailsForOrdersNotifications, $name);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_close($stmt);

        return $this->getSettings($mysqlLink);
    }

    //**************************************************************************

    /**
     * Get list of item sizes
     * @param $mysqlLink Link to DB connection
     * @return \SizeRecord Array of item size records
     */
    function getSizesList($mysqlLink) {
        $query = "SELECT sz.order, sz.name FROM tbl_sizes AS sz ORDER BY sz.order";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $order, $name);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $record = new SizeRecord;
            $record->order = $order;
            $record->name = $name;
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Read single item size record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \SizeRecord Item size record
     */
    function readSize($mysqlLink, $id) {
        $query = "SELECT sz.order, sz.name FROM tbl_sizes AS sz WHERE sz.order = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $order, $name);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $record = new SizeRecord;
            $record->order = $order;
            $record->name = $name;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new item size record
     * @param $mysqlLink Link to DB connection
     * @param $order Size order (used as size record id as well)
     * @param $name Size name (showed to users)
     * @return \SizeRecord New item size record
     */
    function createSize($mysqlLink, $order, $name) {
        $query = "INSERT INTO tbl_sizes (tbl_sizes.order, name) VALUES (?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "is", $order, $name);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readSize($mysqlLink, $order);
    }

    /**
     * Update item size record. Size with order = 0 cannot be changed
     * @param $mysqlLink Link to DB connection
     * @param $order Size current order (used as size record id as well)
     * @param $newOrder Size new order
     * @param $name Size name (showed to users)
     * @return \SizeRecord New item size record
     */
    function updateSize($mysqlLink, $order, $newOrder, $name) {
        if (($order != 0) && (($this->readSize($mysqlLink, $newOrder) == null) || ($order == $newOrder))) {
            $query = "UPDATE tbl_sizes SET tbl_sizes.order = ?, name = ?  WHERE tbl_sizes.order = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "isi", $newOrder, $name, $order);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            return $this->readSize($mysqlLink, $newOrder);
        } else {
            return $this->readSize($mysqlLink, $order);
        }
    }

    /**
     * Delete item size record. Size with order = 0 cannot be deleted
     * @param $mysqlLink Link to DB connection
     * @param $id Record id (size order)
     * @return True if successful 
     */
    function deleteSize($mysqlLink, $id) {
        if ($id != 0) {
            $query = "DELETE FROM tbl_sizes WHERE tbl_sizes.order = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "i", $id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            return true;
        } else {
            return false;
        }
    }

}

/**
 * Class - settings list
 */
class Settings {

    public $minimumTimeFrameSimple;
    public $minimumTimeFrameAdvanced;
    public $orderingStartHour;
    public $orderingStartMinute;
    public $orderingFinishHour;
    public $orderingFinishMinute;
    public $maxNoDepositCostForSimpleOrder;
    public $maxNoDepositCostForAdvancedOrder;
    public $emailsForCustomersNotifications;
    public $emailsForOrdersNotifications;

}

/**
 * Class - item size record
 */
class SizeRecord {

    public $order;
    public $name;

}
