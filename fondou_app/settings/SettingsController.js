//Controller for settings page
app.controller('SettingsController', ['$scope', '$http', function ($scope, $http) {
        //app settings object
        $scope.settings = null;
        //sizes list
        $scope.sizes = [];
        //admin current password
        $scope.oldPassword = '';
        //admin new password
        $scope.newPassword = '';
        //admin new password confirmation
        $scope.newPasswordConfirm = '';

        //***********************************************************************

        //load app settings
        $scope.loadSettings = function () {
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'ReadSettings'
            })
                    .success(function (settings) {
                        $scope.settings = settings;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Settings Error',
                            message: 'Cannot load settings'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //save app settings
        $scope.saveSettings = function () {
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'SaveSettings',
                settings: JSON.stringify($scope.settings)
            })
                    .success(function (settings) {
                        $scope.settings = settings;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Settings Error',
                            message: 'Cannot save settings'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //***********************************************************************

        //load sizes
        $scope.loadSizes = function () {
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'ReadSizes'
            })
                    .success(function (sizes) {
                        //add fields to all sizes
                                for (i = 0; i < sizes.length; i++) {
                            $scope.addFieldsSize(sizes[i]);
                        }
                        //
                        $scope.sizes = sizes;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Size Error',
                            message: 'Cannot load list of sizes'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //add field to size
        $scope.addFieldsSize = function (size) {
            size.newOrder = size.order;
        };

        //create new size, add to sizes list
        $scope.createSize = function () {
            //find max order of existing sizes
            var maxOrder = 0;
            for (i = 0; i < $scope.sizes.length; i++) {
                if ($scope.sizes[i].order > maxOrder) {
                    maxOrder = $scope.sizes[i].order;
                }
            }
            //
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'CreateSize',
                size_order: maxOrder + 10,
                size_name: ''
            })
                    .success(function (newSize) {
                        $scope.addFieldsSize(newSize);
                        $scope.sizes.push(newSize);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Size Error',
                            message: 'Cannot create new size'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update size, show result
        $scope.updateSize = function (index) {
            console.log($scope.sizes[index]);
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'UpdateSize',
                size_order: $scope.sizes[index].order,
                size_new_order: $scope.sizes[index].newOrder,
                size_name: $scope.sizes[index].name
            })
                    .success(function (size) {
                        console.log(size);
                        $scope.addFieldsSize(size);
                        $scope.sizes[index] = size;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Size Error',
                            message: 'Cannot save size'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete size, remove from sizes list
        $scope.deleteSize = function (index) {
            dialogParameters = {
                header: 'Delete Size',
                message: 'Delete Size "' + $scope.sizes[index].name + '"?',
                isDoable: true
            };

            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);

            //delete if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/settings/settings_manager.php", {
                    action: 'DeleteSize',
                    size_order: $scope.sizes[index].order
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.sizes.splice(index, 1);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Size Error',
                                message: 'Cannot delete size'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //update admin password
        $scope.changeAdminPassword = function () {
            dialogParameters = {
                header: 'Change password',
                message: 'Change password?',
                isDoable: true
            };

            //check is new password and confirmation match
            if ($scope.newPassword == $scope.newPasswordConfirm) {
                //show confirmation dialog
                dialog = $scope.confirmationDialog(dialogParameters);
            } 
            //if not show notification
            else {
                dialogParameters = {
                    header: 'Password Error',
                    message: 'New password and confirmation do not match'
                };
                $scope.errorMessageDialog(dialogParameters);
            }

            //if confirmed change password
            dialog.result.then(function () {
                $http.post("/fondou_app/settings/settings_manager.php", {
                    action: 'ChangeAdminPassword',
                    old_password: $scope.oldPassword,
                    new_password: $scope.newPassword
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.oldPassword = '';
                                $scope.newPassword = '';
                                $scope.newPasswordConfirm = '';
                            } 
                            //if change was not successful notify user
                            else {
                                dialogParameters = {
                                    header: 'Password Error',
                                    message: 'Cannot change password. Check the current password'
                                };
                                $scope.errorMessageDialog(dialogParameters);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Password Error',
                                message: 'Cannot change password'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //initialize page
        $scope.loadSettings();
        $scope.loadSizes();
    }]);