<?php

/**
 * Class to implement app settings data model. Client's point of view
 */
class ApiSettingsModel {

    /**
     * Get list of app settings as an object
     * @param $mysqlLink Link to DB connection
     * @return \SettingsApi Settings object
     */
    function getSettings($mysqlLink) {
        $query = "SELECT name, value FROM tbl_settings";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $value);
        mysqli_stmt_store_result($stmt);

        $settings = new SettingsApi();

        while (mysqli_stmt_fetch($stmt)) {
            switch ($name) {
                case "MinimumTimeFrameSimple":
                    $settings->minimumTimeFrameSimple = (integer) $value;
                    break;
                case "MinimumTimeFrameAdvanced":
                    $settings->minimumTimeFrameAdvanced = (integer) $value;
                    break;
                case "OrderingStartHour":
                    $settings->orderingStartHour = (integer) $value;
                    break;
                case "OrderingStartMinute":
                    $settings->orderingStartMinute = (integer) $value;
                    break;
                case "OrderingFinishHour":
                    $settings->orderingFinishHour = (integer) $value;
                    break;
                case "OrderingFinishMinute":
                    $settings->orderingFinishMinute = (integer) $value;
                    break;
                case "MaxNoDepositCostForSimpleOrder":
                    $settings->maxNoDepositCostForSimpleOrder = (integer) $value;
                    break;
                case "MaxNoDepositCostForAdvancedOrder":
                    $settings->maxNoDepositCostForAdvancedOrder = (integer) $value;
                    break;
            }
        }
        mysqli_stmt_close($stmt);

        return $settings;
    }

}

/**
 * Class - response object for client app with server app settings information
 */
class SettingsApi {

    public $minimumTimeFrameSimple;
    public $minimumTimeFrameAdvanced;
    public $orderingStartHour;
    public $orderingStartMinute;
    public $orderingFinishHour;
    public $orderingFinishMinute;
    public $maxNoDepositCostForSimpleOrder;
    public $maxNoDepositCostForAdvancedOrder;

}
