<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './settings_model.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$settingsModel = new SettingsModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$settings = filter_input(INPUT_POST, 'settings');
$sizeOrder = filter_input(INPUT_POST, 'size_order');
$sizeNewOrder = filter_input(INPUT_POST, 'size_new_order');
$sizeName = filter_input(INPUT_POST, 'size_name');
$oldPassword = filter_input(INPUT_POST, 'old_password');
$newPassword = filter_input(INPUT_POST, 'new_password');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadSettings":
            $settings = $settingsModel->getSettings($mysqlLink);
            echo json_encode($settings);
            break;
        case "SaveSettings":
            $newSettings = $settingsModel->saveSettings($mysqlLink, $settings);
            echo json_encode($newSettings);
            break;
        case "ReadSizes":
            $sizesList = $settingsModel->getSizesList($mysqlLink);
            echo json_encode($sizesList);
            break;
        case "CreateSize":
            $newSize = $settingsModel->createSize($mysqlLink, $sizeOrder, $sizeName);
            echo json_encode($newSize);
            break;
        case "UpdateSize":
            $size = $settingsModel->updateSize($mysqlLink, $sizeOrder, $sizeNewOrder, $sizeName);
            echo json_encode($size);
            break;
        case "DeleteSize":
            if ($settingsModel->deleteSize($mysqlLink, $sizeOrder)) {
                echo "SUCCESS";
            }
            break;
        case "ChangeAdminPassword":
            if ($authorization->changeAdminPassword($mysqlLink, $oldPassword, $newPassword)) {
                echo "SUCCESS";
            }
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}