<?php

/**
 * Class to implement menu options data model. Administrator's point of view
 */
class OptionsModel {

    /**
     * Get list of menu options
     * @param $mysqlLink Link to DB connection
     * @return \OptionRecord Array of menu option records
     */
    function getOptionsList($mysqlLink) {
        $query = "SELECT id, name FROM tbl_options ORDER BY name";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name);
        mysqli_stmt_store_result($stmt);
        $result = array();
        while (mysqli_stmt_fetch($stmt)) {
            $record = new OptionRecord;
            $record->id = $id;
            $record->name = $name;
            $result[] = $record;
        }
        mysqli_stmt_close($stmt);

        return $result;
    }
    
    /**
     * Read single menu option record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \OptionRecord Menu option record
     */
    function readOption($mysqlLink, $id) {
        $query = "SELECT name FROM tbl_options WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $record = new OptionRecord;
            $record->id = $id;
            $record->name = $name;
            return $record;
        } else {
            return null;
        }
    }
    
    /**
     * Create new menu option record
     * @param $mysqlLink Link to DB connection
     * @param $name New option name
     * @return \OptionRecord New option record
     */
    function createOption ($mysqlLink, $name) {
        $query = "INSERT INTO tbl_options (name) VALUES (?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $name);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readOption($mysqlLink, $id);
    }
   
    /**
     * Update menu option record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $name Option name
     * @return \OptionRecord Updated option record
     */
    function updateOption($mysqlLink, $id, $name) {
        $query = "UPDATE tbl_options SET name = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "si", $name, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readOption($mysqlLink, $id);
    }
    
    /**
     * Check if menu option record can be deleted (option has no values or associated categories)
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if can be deleted 
     */
    function checkOptionDeletable($mysqlLink, $id) {
        $query = "SELECT id FROM tbl_options_values WHERE option_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRowsValues = $stmt->num_rows;
        mysqli_stmt_close($stmt);
        
        $query = "SELECT id FROM tbl_categories_options WHERE option_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $numRowsCategories= $stmt->num_rows;
        mysqli_stmt_close($stmt);

        if (($numRowsValues > 0) || ($numRowsCategories > 0)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Delete menu option record. Will throw exception if option has values or associated categories
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteOption($mysqlLink, $id) {
        if (!$this->checkOptionDeletable($mysqlLink, $id)) {
            throw new Exception('Cannot delete an Option with Values or Categories', 15);
        }
        $query = "DELETE FROM tbl_options WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }
}

/**
 * Class - menu option record for dashboard
 */
class OptionRecord {

    public $id;
    public $name;

}