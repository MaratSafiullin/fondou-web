//Controller for options page
app.controller('OptionsController', ['$scope', '$http', function ($scope, $http) {
        //selected option
        $scope.currentOption = null;
        //all options list
        $scope.options = [];
        //values for selected option
        $scope.values = [];
        //associated menu categories for selected option
        $scope.optCategories = [];
        //all menu categories list
        $scope.menuCategories = [];

        //***********************************************************************

        //load menu options
        $scope.loadOptions = function () {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'ReadOptions'
            })
                    .success(function (options) {
                        $scope.options = options;
                        //select first option in list if there is any
                        if (options.length > 0) {
                            $scope.currentOption = options[0];
                            //load values and associated categories
                            $scope.selectOption();
                        }

                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Option Error',
                            message: 'Cannot load list of options'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //load values and associated categories for selected category
        $scope.selectOption = function () {
            $scope.loadValues();
            $scope.loadOptCategories();
        };

        //create new menu option, add to options list
        $scope.createOption = function () {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'CreateOption',
                option_name: ''
            })
                    .success(function (newOption) {
                        $scope.options.push(newOption);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Option Error',
                            message: 'Cannot create new option'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update menu option, show result
        $scope.updateOption = function (index) {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'UpdateOption',
                option_id: $scope.options[index].id,
                option_name: $scope.options[index].name
            })
                    .success(function (option) {
                        $scope.options[index] = option;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Option Error',
                            message: 'Cannot save option'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete menu option if possible, remove from options list
        $scope.deleteOption = function (index) {
            //check if can be deleted
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'CheckOptionDeletable',
                option_id: $scope.options[index].id
            })
                    .success(function (result) {
                        //if can be deleted ask for confirmation
                        if (result === "YES") {
                            dialogParameters = {
                                header: 'Delete Option',
                                message: 'Delete Option "' + $scope.options[index].name + '"?',
                                isDoable: true
                            };
                        }
                        //if not - notify user
                        else {
                            dialogParameters = {
                                header: 'Delete Option',
                                message: 'Option "' + $scope.options[index].name + '" has values or categories. It cannot be deleted',
                                isDoable: false
                            };
                        }

                        //show confirmation dialog
                        dialog = $scope.confirmationDialog(dialogParameters);

                        //delete if confirmed
                        dialog.result.then(function () {
                            $http.post("/fondou_app/options/options_manager.php", {
                                action: 'DeleteOption',
                                option_id: $scope.options[index].id
                            })
                                    .success(function (result) {
                                        if (result === "SUCCESS") {
                                            $scope.options.splice(index, 1);
                                        }
                                        //select first option in list if there is any
                                        if ($scope.options.length > 0) {
                                            $scope.currentOption = $scope.options[0];
                                            //load values and associated categories
                                            $scope.selectOption();
                                        }
                                    })
                                    .error(function (err) {
                                        dialogParameters = {
                                            header: 'Option Error',
                                            message: 'Cannot delete option'
                                        };
                                        $scope.errorMessageDialog(dialogParameters);
                                    });
                        });
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Option Error',
                            message: 'Cannot check if option can be deleted'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //***********************************************************************

        //load option values for selected option
        $scope.loadValues = function () {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'ReadValues',
                option_id: $scope.currentOption.id
            })
                    .success(function (values) {
                        $scope.values = values;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Value Error',
                            message: 'Cannot load list of values'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //create new option value of selected option, add to values list
        $scope.createValue = function () {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'CreateValue',
                option_id: $scope.currentOption.id,
                value_order: '',
                value_value: '',
                value_price_modifier: ''
            })
                    .success(function (newValue) {
                        $scope.values.push(newValue);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Value Error',
                            message: 'Cannot create new value'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //update option value, show result
        $scope.updateValue = function (index) {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'UpdateValue',
                value_id: $scope.values[index].id,
                value_order: $scope.values[index].order,
                value_value: $scope.values[index].value,
                value_price_modifier: $scope.values[index].priceModifier
            })
                    .success(function (value) {
                        $scope.values[index] = value;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Value Error',
                            message: 'Cannot save value'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //delete option value, remove from values list
        $scope.deleteValue = function (index) {
            dialogParameters = {
                header: 'Delete Value',
                message: 'Delete Value "' + $scope.values[index].value + '"?',
                isDoable: true
            };

            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);

            //delete if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/options/options_manager.php", {
                    action: 'DeleteValue',
                    value_id: $scope.values[index].id
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.values.splice(index, 1);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Value Error',
                                message: 'Cannot delete value'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //load associated categories for selected option
        $scope.loadOptCategories = function () {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'ReadOptCategories',
                option_id: $scope.currentOption.id
            })
                    .success(function (optCategories) {
                        $scope.optCategories = optCategories;
                        //find matching category objects
                        for (i = 0; i < optCategories.length; i++) {
                            $scope.findCategoryForOptCategory(optCategories[i]);
                        }
                        //
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot load list of option categories'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //add associated category for selected option, add to associated categories list
        //will not be created if option is already asociated with all categories
        $scope.addOptCategory = function () {
            for (i = 0; i < $scope.menuCategories.length; i++) {
                //find first category that is not yet associated with option
                var presentInOptCategories = false;
                for (j = 0; j < $scope.optCategories.length; j++) {
                    if ($scope.optCategories[j].categoryId === $scope.menuCategories[i].id) {
                        presentInOptCategories = true;
                        break;
                    }
                }
                //if there is any create new
                if (!presentInOptCategories) {
                    $http.post("/fondou_app/options/options_manager.php", {
                        action: 'AddOptCategory',
                        option_id: $scope.currentOption.id,
                        category_id: $scope.menuCategories[i].id
                    })
                            .success(function (newOptCategory) {
                                if (newOptCategory !== 'null') {
                                    //find matching category object
                                    $scope.findCategoryForOptCategory(newOptCategory);
                                    //
                                    $scope.optCategories.push(newOptCategory);
                                }
                            })
                            .error(function (err) {
                                dialogParameters = {
                                    header: 'Category Error',
                                    message: 'Cannot add option category'
                                };
                                $scope.errorMessageDialog(dialogParameters);
                            });
                    break;
                }
            }
        };

        //update associated category, show result
        $scope.changeOptCategory = function (index) {
            $http.post("/fondou_app/options/options_manager.php", {
                action: 'ChangeOptCategory',
                opt_category_id: $scope.optCategories[index].id,
                category_id: $scope.optCategories[index].category.id
            })
                    .success(function (optCategory) {
                        //find matching category object
                        $scope.findCategoryForOptCategory(optCategory);
                        //
                        $scope.optCategories[index] = optCategory;
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot save option category'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //remove associated category (delete association record), remove from associated categories list
        $scope.removeOptCategory = function (index) {
            dialogParameters = {
                header: 'Remove Category',
                message: 'Remove Category "' + $scope.optCategories[index].category.name + '"?',
                isDoable: true
            };

            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);

            //delete if confirmed
            dialog.result.then(function () {
                $http.post("/fondou_app/options/options_manager.php", {
                    action: 'RemoveOptCategory',
                    opt_category_id: $scope.optCategories[index].id
                })
                        .success(function (result) {
                            if (result === "SUCCESS") {
                                $scope.optCategories.splice(index, 1);
                            }
                        })
                        .error(function (err) {
                            dialogParameters = {
                                header: 'Category Error',
                                message: 'Cannot remove category'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        });
            });
        };

        //***********************************************************************

        //load list of menu categories
        $scope.loadCategories = function () {
            $http.post("/fondou_app/menu/menu_manager.php", {
                action: 'ReadCategories'
            })
                    .success(function (menuCategories) {
                        $scope.menuCategories = menuCategories;
                        //find matching category objects for option-category associations
                        for (i = 0; i < $scope.optCategories.length; i++) {
                            $scope.findCategoryForOptCategory($scope.optCategories[i]);
                        }
                        //
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Category Error',
                            message: 'Cannot load list of categories'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //find matching category object for option-category association
        //used to handle category selection "<select>"
        $scope.findCategoryForOptCategory = function (optCategory) {
            for (j = 0; j < $scope.menuCategories.length; j++) {
                if ($scope.menuCategories[j].id === optCategory.categoryId) {
                    optCategory.category = $scope.menuCategories[j];
                    break;
                }
            }
        };

        //***********************************************************************

        //initialize page
        $scope.loadOptions();
        $scope.loadCategories();

    }]);