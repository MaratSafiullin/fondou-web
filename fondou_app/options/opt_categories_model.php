<?php

/**
 * Class to implement menu category-to-option associations data model. Administrator's point of view 
 */
class OptCategoriesModel {

    /**
     * Get list of category-to-option associations for an option
     * @param $mysqlLink Link to DB connection
     * @param $optionId Option Id
     * @return \OptCategoryRecord Array of association records
     */
    function getOptCategoriesList($mysqlLink, $optionId) {
        $query = "SELECT id, category_id FROM tbl_categories_options WHERE option_id = ? ORDER BY id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $optionId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $categoryId);
        mysqli_stmt_store_result($stmt);
        $result = array();
        while (mysqli_stmt_fetch($stmt)) {
            $record = new OptCategoryRecord();
            $record->id = $id;
            $record->categoryId = $categoryId;
            $result[] = $record;
        }
        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Read single category-to-option association record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \OptCategoryRecord Association record
     */
    function readOptCategory($mysqlLink, $id) {
        $query = "SELECT id, category_id FROM tbl_categories_options WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $categoryId);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $record = new OptCategoryRecord();
            $record->id = $id;
            $record->categoryId = $categoryId;
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new category-to-option association record
     * @param $mysqlLink Link to DB connection
     * @param $optionId Option Id
     * @param $categoryId Category Id
     * @return \OptCategoryRecord New association record
     */
    function addOptCategory($mysqlLink, $optionId, $categoryId) {
        $query = "INSERT INTO tbl_categories_options (option_id, category_id) VALUES (?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ii", $optionId, $categoryId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readOptCategory($mysqlLink, $id);
    }

    /**
     * Update category-to-option association record (change category Id)
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $categoryId Category Id
     * @return \OptCategoryRecord Updated association record
     */
    function changeOptCategory($mysqlLink, $id, $categoryId) {
        $query = "UPDATE tbl_categories_options SET category_id = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ii", $categoryId, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readOptCategory($mysqlLink, $id);
    }

    /**
     * Delete category-to-option association record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function removeOptCategory($mysqlLink, $id) {
        $query = "DELETE FROM tbl_categories_options WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

}

/**
 * Class - menu category-to-option association record for dashboard
 */
class OptCategoryRecord {

    public $id;
    public $categoryId;

}