<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './options_model.php';
include_once './values_model.php';
include_once './opt_categories_model.php';
include_once '../menu/categories_model.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$optionsModel = new OptionsModel;
$valuesModel = new ValuesModel;
$optCategoriesModel = new OptCategoriesModel;
$categoriesModel = new CategoriesModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$optionId = filter_input(INPUT_POST, 'option_id');
$optionName = filter_input(INPUT_POST, 'option_name');
$valueId = filter_input(INPUT_POST, 'value_id');
$valueOrder = filter_input(INPUT_POST, 'value_order');
$valueValue = filter_input(INPUT_POST, 'value_value');
$valuePriceModifier = filter_input(INPUT_POST, 'value_price_modifier');
$optCategoryId = filter_input(INPUT_POST, 'opt_category_id');
$categoryId = filter_input(INPUT_POST, 'category_id');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadOptions":
            $optionsList = $optionsModel->getOptionsList($mysqlLink);
            echo json_encode($optionsList);
            break;
        case "CreateOption":
            $newOption = $optionsModel->createOption($mysqlLink, $optionName);
            echo json_encode($newOption);
            break;
        case "UpdateOption":
            $option = $optionsModel->updateOption($mysqlLink, $optionId, $optionName);
            echo json_encode($option);
            break;
        case "CheckOptionDeletable":
            if ($optionsModel->checkOptionDeletable($mysqlLink, $optionId)) {
                echo "YES";
            } else {
                echo "NO";
            }
            break;
        case "DeleteOption":
            if ($optionsModel->deleteOption($mysqlLink, $optionId)) {
                echo "SUCCESS";
            }
            break;
        case "ReadValues":
            $valuesList = $valuesModel->getValuesList($mysqlLink, $optionId);
            echo json_encode($valuesList);
            break;
        case "CreateValue":
            $newValue = $valuesModel->createValue($mysqlLink, $optionId, $valueOrder, $valueValue, $valuePriceModifier);
            echo json_encode($newValue);
            break;
        case "UpdateValue":
            $value = $valuesModel->updateValue($mysqlLink, $valueId, $valueOrder, $valueValue, $valuePriceModifier);
            echo json_encode($value);
            break;
        case "DeleteValue":
            if ($valuesModel->deleteValue($mysqlLink, $valueId)) {
                echo "SUCCESS";
            }
            break;
        case "ReadOptCategories":
            $optCategoriesList = $optCategoriesModel->getOptCategoriesList($mysqlLink, $optionId);
            echo json_encode($optCategoriesList);
            break;    
        case "AddOptCategory":
            $newOptCategory = $optCategoriesModel->addOptCategory($mysqlLink, $optionId, $categoryId);
            echo json_encode($newOptCategory);
            break;
        case "UpdateOptCategory":
            $optCategory = $optCategoriesModel->updateOptCategory($mysqlLink, $optCategoryId, $categoryId);
            echo json_encode($optCategory);
            break;
        case "ChangeOptCategory":
            $optCategory = $optCategoriesModel->changeOptCategory($mysqlLink, $optCategoryId, $categoryId);
            echo json_encode($optCategory);
            break;
        case "RemoveOptCategory":
            if ($optCategoriesModel->removeOptCategory($mysqlLink, $optCategoryId)) {
                echo "SUCCESS";
            }
            break;    
        case "ReadCategories":
            $categoriesList = $categoriesModel->getCategoriesList($mysqlLink);
            echo json_encode($categoriesList);
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->description = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}