<?php

/**
 * Class to implement menu option values data model. Administrator's point of view
 */
class ValuesModel {

    /**
     * Get list of values for an option
     * @param $mysqlLink Link to DB connection
     * @param $optionId Option Id
     * @return \ValueRecord Array of value records
     */
    function getValuesList($mysqlLink, $optionId) {
        $query = "SELECT val.id, val.order, val.value, val.price_modifier "
                . "FROM tbl_options_values AS val "
                . "WHERE option_id = ? "
                . "ORDER BY val.order, val.id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $optionId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $order, $value, $priceModifier);
        mysqli_stmt_store_result($stmt);
        $result = array();
        while (mysqli_stmt_fetch($stmt)) {
            $record = new ValueRecord();
            $record->id = $id;
            $record->order = $order;
            $record->value = $value;
            $record->priceModifier = $priceModifier / 100; //price modifier in DB is kept as cents integer value
            $result[] = $record;
        }
        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Read single value record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \ValueRecord Value record
     */
    function readValue($mysqlLink, $id) {
        $query = "SELECT val.id, val.order, val.value, val.price_modifier FROM tbl_options_values AS val WHERE val.id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $order, $value, $priceModifier);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $record = new ValueRecord();
            $record->id = $id;
            $record->order = $order;
            $record->value = $value;
            $record->priceModifier = $priceModifier / 100;  //price modifier in DB is kept as cents integer value
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Create new value record
     * @param $mysqlLink Link to DB connection
     * @param $optionId Value (parent) option ID
     * @param $order Value order number (needed to sort values)
     * @param $value Value string representation
     * @param $priceModifier Price modifier to change menu item total price if value is chosen
     * @return \ValueRecord New value record
     */
    function createValue($mysqlLink, $optionId, $order, $value, $priceModifier) {
        $priceModifier = (integer) ($priceModifier * 100); //price modifier in DB is kept as cents integer value
        $query = "INSERT INTO tbl_options_values (option_id, tbl_options_values.order, value, price_modifier) VALUES (?, ?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "iisi", $optionId, $order, $value, $priceModifier);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $id = mysqli_insert_id($mysqlLink);

        return $this->readValue($mysqlLink, $id);
    }

    /**
     * Update new value record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $order Value order number (needed to sort values)
     * @param $value Value string representation
     * @param $priceModifier Price modifier to change menu item total price if value is chosen
     * @return \ValueRecord Updated value record
     */
    function updateValue($mysqlLink, $id, $order, $value, $priceModifier) {
        $priceModifier = (integer) ($priceModifier * 100); //price modifier in DB is kept as cents integer value
        $query = "UPDATE tbl_options_values SET tbl_options_values.order = ?, value = ?, price_modifier = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ssii", $order, $value, $priceModifier, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readValue($mysqlLink, $id);
    }

    /**
     * Delete value record
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return True if successful 
     */
    function deleteValue($mysqlLink, $id) {
        $query = "DELETE FROM tbl_options_values WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return true;
    }

}

/**
 * Class - option value record for dashboard
 */
class ValueRecord {

    public $id;
    public $order;
    public $value;
    public $priceModifier;

}