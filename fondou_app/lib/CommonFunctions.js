/**
 * Convert MySql time string to JS Date object
 * @param {String} mysqlTime DateTime value in MySql format
 * @returns {Date} JS Date
 */
function MysqlTimeToTime(mysqlTime) {
    // Split timestamp into [ Y, M, D, h, m, s ]
    var t = mysqlTime.split(/[- :]/);
    // Apply each element to the Date function
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    return d;
}