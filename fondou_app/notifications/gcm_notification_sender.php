<?php

/**
 * Class - GCM notifications sender
 */
class GcmNotificationSender {

    function sendNotification($mysqlLink, $order) {
        // API access key from Google API's Console
        define('API_ACCESS_KEY', 'AIzaSyDG4satOvaZpClFwX13LRsaXYjyuGJ_lis');
        define('GCM_URL', 'https://android.googleapis.com/gcm/send');

        //get GCM IDs to notify
        $customerId = $order->customerId;
        $query = "SELECT gcm_id FROM tbl_gcm_ids WHERE customer_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $customerId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $gcmId);
        mysqli_stmt_store_result($stmt);
        $gcmIds = array();
        while (mysqli_stmt_fetch($stmt)) {
            $gcmIds[] = $gcmId;
        }
        mysqli_stmt_close($stmt);

        //get possible order statuses
        $ordersModel = new OrdersModel;
        $statuses = $ordersModel->getOrderStatuses();
        $finalStatuses = $ordersModel->getOrderFinalStatuses();
        
        //prepare message
        $orderTime = strtotime($order->time);
        $msgText = date('d/M/y h:i A', $orderTime);
        switch ($order->status) {
            case $statuses->processed:
                    $msgText = $msgText . ' order confirmed';
                break;
            case $statuses->sent:
                    $msgText = $msgText . ' order ready to pick up';
                break;
            case $finalStatuses->canceledAdmin:
                    $msgText = $msgText . ' order cancelled';
                break;
        }
        $msg = array(
            'message' => $msgText,
            'title' => 'Order status changed',
            'vibrate' => 1,
            'sound' => 1
        );
        $fields = array(
            'registration_ids' => $gcmIds,
            'data' => $msg
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        //send message
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        
        //delete invalid GCM IDs
        $authorization = new Authorization;
        $result = json_decode($result);
        for ($i = 0; $i < count($result->results); $i++) {
            if ($result->results[$i]->message_id == null) {
                $authorization->deleteGcmId($mysqlLink, $customerId, $gcmIds[$i]);
            }
        }
    }

}
