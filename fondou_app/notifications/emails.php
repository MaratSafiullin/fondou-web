<?php

/**
 * Class - email notifications sender
 */
class EmailSender {

    /**
     * Send notification about new customer registration
     * @param $mysqlLink Link to DB connection
     * @param $customer Customer record object
     */
    function newCustomerEmail($mysqlLink, $customer) {
        //get emails to notify
        $settingsModel = new SettingsModel;
        $settings = $settingsModel->getSettings($mysqlLink);
        $emails = explode(" ", $settings->emailsForCustomersNotifications);

        //prepare message
        $emailBody = "New cutomer registered.\n\n"
                . "Name: {$customer->name}\n"
                . "Phone: {$customer->phone}\n"
                . "Email: {$customer->email}\n";
        $head = "From: Fondou Website Robot <robot@fondou.nz>\n"
                . "Subject: New customer - {$customer->name}";
        //send emails
        foreach ($emails as $email) {
            $email = trim($email);
            $mailSent = mail($email, "New customer - {$customer->name}", $emailBody, $head);
        }
    }

    /**
     * Send notification about new order
     * @param $mysqlLink Link to DB connection
     * @param $customer Customer record object (order sender)
     * @param $newOrder Order record object
     */
    function newOrdersEmail($mysqlLink, $customer, $newOrder) {
        //get emails to notify
        $settingsModel = new SettingsModel;
        $settings = $settingsModel->getSettings($mysqlLink);
        $emails = explode(" ", $settings->emailsForOrdersNotifications);

        //prepare message
        $price = $newOrder->totalPrice / 100;
        $emailBody = "New order posted.\n\n"
                . "Customer Name: {$customer->name}\n"
                . "Customer Phone: {$customer->phone}\n"
                . "Customer Email: {$customer->email}\n"
                . "Order Time: {$newOrder->orderTime}\n"
                . "Pickup Time: {$newOrder->deliveryTime}\n"
                . "Order Price: {$price}\n\n"
                . "Items:\n";
        foreach ($newOrder->items as $item) {
            $price = $item->price / 100;
            $emailBody .= "\t{$item->count} {$item->name} ({$item->description}) {$price}\n";
        }
        $head = "From: Fondou Website Robot <robot@fondou.nz>\n"
                . "Subject: New order";
        //send emails
        foreach ($emails as $email) {
            $email = trim($email);
            $mailSent = mail($email, "New order", $emailBody, $head);
        }
    }

}
