<?php

//allowed order statuses
        const STATUS_CREATED = "created", STATUS_PROCESSED = "processed",
        STATUS_SENT = "sent", STATUS_FINISHED = "finished";
        const STATUS_CANCELED_CUSTOMER = "canceled_customer", STATUS_CANCELED_ADMIN = "canceled_admin";

/**
 * Class to implement customer orders data model. Administrator's point of view
 */
class OrdersModel {

    /**
     * Get order changeable statuses list
     * @return \StatusesList Statuses list
     */
    function getOrderStatuses() {
        return new StatusesList;
    }

    /**
     * Get order final (unchangeable) statuses list
     * @return \StatusesList Statuses list
     */
    function getOrderFinalStatuses() {
        return new FinalStatusesList;
    }

    /**
     * Get total number of orders with a particular status
     * @param $mysqlLink Link to DB connection
     * @param $status Orders-to-count status 
     * @return Orders count
     */
    function getOrdersCount($mysqlLink, $status) {
        $query = "SELECT COUNT(id) FROM tbl_orders WHERE status = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $status);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $count);
        mysqli_stmt_store_result($stmt);
        mysqli_stmt_fetch($stmt);
        return $count;
    }

    /**
     * Get list of orders with a particular status. Order record includes order items records
     * @param $mysqlLink Link to DB connection
     * @param $status Orders-to-select status 
     * @return \OrderRecord Array or order records
     */
    function getOrdersList($mysqlLink, $status) {
        $query = "SELECT ord.id, cst.phone, cst.name, ord.time, ord.address, ord.status, ord.delivery_time, ord.total_price, ord.advanced "
                . "FROM tbl_orders AS ord "
                . "INNER JOIN tbl_customers AS cst ON cst.id = ord.customer_id "
                . "WHERE ord.status = ? "
                . "ORDER BY timestamp ";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "s", $status);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $customerPhone, $customerName, $time, $address, $status, $deliveryTime, $totalPrice, $advanced);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $advanced = (boolean) $advanced;
            $record = new OrderRecord;
            $record->id = $id;
            $record->advanced = $advanced;
            $record->customerPhone = $customerPhone;
            $record->customerName = $customerName;
            $record->time = $time;
            $record->deliveryTime = $deliveryTime;
            $record->address = $address;
            $record->status = $status;
            $record->items = $this->getItemsList($mysqlLink, $id);
            $record->totalPrice = $totalPrice / 100; //price in DB is kept as cents integer value
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Get (part of a) list of orders with a particular status. Order record includes order items records
     * Result list starts from a given record number and includes no more than a given number of records
     * @param $mysqlLink Link to DB connection
     * @param $status Orders-to-select status 
     * @param $startRow Start record number
     * @param $rowCount Number of records to include
     * @return \OrderRecord Array or order records
     */
    function getOrdersListWithConditions($mysqlLink, $status, $startRow, $rowCount) {
        if (($startRow != null) && ($rowCount != null)) {
            $limitCondition = "LIMIT ?, ? ";
        } else {
            $limitCondition = "";
        }
        $query = "SELECT ord.id, cst.phone, cst.name, ord.time, ord.address, ord.status, ord.delivery_time, ord.total_price, ord.advanced "
                . "FROM tbl_orders AS ord "
                . "INNER JOIN tbl_customers AS cst ON cst.id = ord.customer_id "
                . "WHERE ord.status = ? "
                . "ORDER BY timestamp DESC "
                . $limitCondition;
        $stmt = mysqli_prepare($mysqlLink, $query);
        if (($startRow != null) && ($rowCount != null)) {
            mysqli_stmt_bind_param($stmt, "sii", $status, $startRow, $rowCount);
        } else {
            mysqli_stmt_bind_param($stmt, "s", $status);
        }
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $customerPhone, $customerName, $time, $address, $status, $deliveryTime, $totalPrice, $advanced);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $advanced = (boolean) $advanced;
            $record = new OrderRecord;
            $record->id = $id;
            $record->advanced = $advanced;
            $record->customerPhone = $customerPhone;
            $record->customerName = $customerName;
            $record->time = $time;
            $record->deliveryTime = $deliveryTime;
            $record->address = $address;
            $record->status = $status;
            $record->items = $this->getItemsList($mysqlLink, $id);
            $record->totalPrice = $totalPrice / 100; //price in DB is kept as cents integer value
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Get orders list "page" record - part of orders list selected by status
     * Record includes total (selected by status) records number, that can be used to calculate page's number etc.
     * @param $mysqlLink Link to DB connection
     * @param $status Orders-to-select status 
     * @param $startRow Start record number
     * @param $rowCount Number of records to include
     * @return \OrdersPage Page record
     */
    function getOrdersPage($mysqlLink, $status, $startRow, $rowCount) {
        $page = new OrdersPage;
        $page->count = $this->getOrdersCount($mysqlLink, $status);
        $page->ordersList = $this->getOrdersListWithConditions($mysqlLink, $status, $startRow, $rowCount);
        return $page;
    }

    /**
     * Read single order record. Order record includes order items records
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return \OrderRecord Order record
     */
    function readOrder($mysqlLink, $id) {
        $query = "SELECT ord.id, cst.phone, cst.name, ord.time, ord.address, ord.status, ord.delivery_time, ord.total_price, ord.advanced, ord.customer_id "
                . "FROM tbl_orders AS ord "
                . "INNER JOIN tbl_customers AS cst ON cst.id = ord.customer_id "
                . "WHERE ord.id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $customerPhone, $customerName, $time, $address, $status, $deliveryTime, $totalPrice, $advanced, $customerId);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $advanced = (boolean) $advanced;
            $record = new OrderRecord;
            $record->id = $id;
            $record->advanced = $advanced;
            $record->customerId = $customerId;
            $record->customerPhone = $customerPhone;
            $record->customerName = $customerName;
            $record->time = $time;
            $record->deliveryTime = $deliveryTime;
            $record->address = $address;
            $record->status = $status;
            $record->items = $this->getItemsList($mysqlLink, $id);
            $record->totalPrice = $totalPrice / 100; //price in DB is kept as cents integer value
            return $record;
        } else {
            return null;
        }
    }

    /**
     * Get list of order items for an order
     * @param $mysqlLink Link to DB connection
     * @param $orderId Order Id
     * @return \OrderItemRecord Array or order item records
     */
    function getItemsList($mysqlLink, $orderId) {
        $query = "SELECT id, name, price, count, description FROM tbl_orders_items WHERE order_id = ? ORDER BY id";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $orderId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $name, $price, $count, $description);
        mysqli_stmt_store_result($stmt);

        $result = array();

        while (mysqli_stmt_fetch($stmt)) {
            $record = new OrderItemRecord;
            $record->id = $id;
            $record->count = $count;
            $record->name = $name;
            $record->description = $description;
            $record->price = $price / 100; //price in DB is kept as cents integer value
            $result[] = $record;
        }

        mysqli_stmt_close($stmt);

        return $result;
    }

    /**
     * Update order record. Set new status if it's allowed (for given order lifecycle model)
     * May throw exceptions if attepmted status change is not allowed
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $newStatus New order status
     * @return Updated order status
     */
    function changeOrderStatus($mysqlLink, $id, $newStatus) {
        $currentStatus = $this->readOrderStatus($mysqlLink, $id);

        //check order lifecycle rules
        if ($currentStatus !== null) {
            //cannot change status for cancelled or finished order
            if (($currentStatus === STATUS_CANCELED_ADMIN) || ($currentStatus === STATUS_CANCELED_CUSTOMER) ||
                    ($currentStatus === STATUS_FINISHED)) {
                throw new Exception('Order current status can`t be changed');
            }
            //cannot change status to unknown status (not allowed by this data model)
            else if (($newStatus !== STATUS_CREATED) && ($newStatus !== STATUS_PROCESSED) &&
                    ($newStatus !== STATUS_SENT) && ($newStatus !== STATUS_FINISHED)) {
                throw new Exception('Unknown order status');
            }
        } else {
            throw new Exception('Cannot get order current status');
        }

        //(QUESTIONABLE) currently order status change is allowed only to change status foreward step by step
        // (cannot skip status, cannot go back to previous status)
        if (($currentStatus === STATUS_CREATED) && ($newStatus !== STATUS_PROCESSED) ||
                ($currentStatus === STATUS_PROCESSED) && ($newStatus !== STATUS_SENT) ||
                ($currentStatus === STATUS_SENT) && ($newStatus !== STATUS_FINISHED)) {
            return $currentStatus;
        }

        $query = "UPDATE tbl_orders SET status = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "si", $newStatus, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readOrderStatus($mysqlLink, $id);
    }

    /**
     * Update order record. Set "canceled by admin" or  status if it's allowed (for given order lifecycle model)
     * May throw exceptions if attepmted status change is not allowed
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @param $newStatus New order status
     * @return Updated order status
     */
    function cancelOrder($mysqlLink, $id, $newStatus) {
        $currentStatus = $this->readOrderStatus($mysqlLink, $id);

        //check order lifecycle rules
        if ($currentStatus !== null) {
            //cannot change status for cancelled or finished order
            if (($currentStatus === STATUS_CANCELED_ADMIN) || ($currentStatus === STATUS_CANCELED_CUSTOMER) ||
                    ($currentStatus === STATUS_FINISHED)) {
                throw new Exception('Order current status can`t be changed');
            }
            //or can be cancelled by customer only if it is stinn new (pending) order
            else if (($newStatus === STATUS_CANCELED_CUSTOMER) && ($currentStatus !== STATUS_CREATED)) {
                throw new Exception('Customer can cancel only not processed order');
            }
            //cannot change status to illegal (not allowed by this data model)
            else if (($newStatus !== STATUS_CANCELED_ADMIN) && ($newStatus !== STATUS_CANCELED_CUSTOMER)) {
                throw new Exception('Unknown order new status');
            }
        } else {
            throw new Exception('Cannot get order current status');
        }

        $query = "UPDATE tbl_orders SET status = ? WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "si", $newStatus, $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        return $this->readOrderStatus($mysqlLink, $id);
    }

    /**
     * Get order record status
     * @param $mysqlLink Link to DB connection
     * @param $id Record id
     * @return Order status (string)
     */
    function readOrderStatus($mysqlLink, $id) {
        $query = "SELECT status FROM tbl_orders WHERE id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "i", $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $currentStatus);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            return $currentStatus;
        } else {
            return null;
        }
    }

}

/**
 * Class - order statuses list: normal flow
 */
class StatusesList {

    public $created = STATUS_CREATED;
    public $processed = STATUS_PROCESSED;
    public $sent = STATUS_SENT;
    public $finished = STATUS_FINISHED;

}

/**
 * Class - order statuses list: ungchangeable final statuses
 */
class FinalStatusesList {

    public $finished = STATUS_FINISHED;
    public $canceledAdmin = STATUS_CANCELED_ADMIN;
    public $canceledCustomer = STATUS_CANCELED_CUSTOMER;

}

/**
 * Class - customer order record for dashboard
 */
class OrderRecord {

    public $id;
    public $advanced;
    public $customerId;
    public $customerPhone;
    public $customerName;
    public $time;
    public $deliveryTime;
    public $address;
    public $status;
    public $totalPrice;
    public $items = array();

}

/**
 * Class - order item record for dashboard
 */
class OrderItemRecord {

    public $id;
    public $count;
    public $name;
    public $description;
    public $price;

}

/**
 * Class - customer orders page record for dashboard
 */
class OrdersPage {

    public $count;
    public $ordersList = array();

}
