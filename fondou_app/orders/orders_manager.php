<?php

include_once '../common/error_handler.php';
include_once '../common/database_access.php';
include_once '../common/authorization.php';
include_once './orders_model.php';
include_once '../notifications/gcm_notification_sender.php';

//prepare to work with data model
$authorization = new Authorization;
$databaseAccess = new DatabaseAccess;
$ordersModel = new OrdersModel;

//check admin autorization
if (!$authorization->checkAdminSession()) {
    exit;
}

//get request parameters
$action = filter_input(INPUT_POST, 'action');
$orderId = filter_input(INPUT_POST, 'order_id');
$orderStatus = filter_input(INPUT_POST, 'order_status');
$startRow = filter_input(INPUT_POST, 'start_row');
$rowCount = filter_input(INPUT_POST, 'row_count');

//find action, return result
try {
    successfulResponse();
    $mysqlLink = $databaseAccess->dbOpen();
    
    switch ($action) {
        case "ReadStatuses":
            $statusesList = $ordersModel->getOrderStatuses();
            echo json_encode($statusesList);
            break;
        case "ReadFinalStatuses":
            $statusesList = $ordersModel->getOrderFinalStatuses();
            echo json_encode($statusesList);
            break;
        case "ReadOrders":
            $ordersList = $ordersModel->getOrdersList($mysqlLink, $orderStatus);
            echo json_encode($ordersList);
            break;
        case "ReadOrdersPage":
            $ordersPage = $ordersModel->getOrdersPage($mysqlLink, $orderStatus, $startRow, $rowCount);
            echo json_encode($ordersPage);
            break;
        case "ChangeOrderStatus":
            $status = $ordersModel->changeOrderStatus($mysqlLink, $orderId, $orderStatus);
            echo $status;
            //send GCM notification to customer's devices
            if (($status === STATUS_PROCESSED) || ($status === STATUS_SENT)) {
                try {
                    $gcmNotificationSender = new GcmNotificationSender();
                    $gcmNotificationSender->sendNotification($mysqlLink, $ordersModel->readOrder($mysqlLink, $orderId));
                } catch (Exception $e) {
                    //ignore
                }
            }
            break;
        case "CancelOrder":
            $status = $ordersModel->cancelOrder($mysqlLink, $orderId, STATUS_CANCELED_ADMIN);
            echo $status;
            //send GCM notification to customer's devices
            try {
                $gcmNotificationSender = new GcmNotificationSender();
                $gcmNotificationSender->sendNotification($mysqlLink, $ordersModel->readOrder($mysqlLink, $orderId));
            } catch (Exception $e) {
                //ignore
            }
            break;
    }
    
    $databaseAccess->dbClose($mysqlLink);
} 
//in case of exception return exception result and HTTP error code
catch (Exception $e) {
    $errorData = new ErrorData;
    errorResponse();

    if ($mysqlLink != null) {
        $errorData->description = $e->getMessage();
        $errorData->stacktrace = $e->getTrace();
    } else {
        $errorData->error = "Cannot connect to Database";
    }
    echo json_encode($errorData);
}