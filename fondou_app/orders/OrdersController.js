//Controller for orders page
app.controller('OrdersController', ['$scope', '$http', function ($scope, $http) {
        //possible order statuses list
        $scope.orderStatuses = [];
        //new (pending) orders list
        $scope.ordersNew = [];
        //orders being processed list
        $scope.ordersProcessed = [];
        //sent/ready to pick up orders list
        $scope.ordersSent = [];
        //app settings object
        $scope.settings = null;

        //***********************************************************************

        //load possible statuses and 3 orders lists
        $scope.initialize = function () {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'ReadStatuses'
            })
                    .success(function (statusesList) {
                        $scope.orderStatuses = statusesList;
                        //load orders lists
                        $scope.loadOrders($scope.orderStatuses.created);
                        $scope.loadOrders($scope.orderStatuses.processed);
                        $scope.loadOrders($scope.orderStatuses.sent);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot load statuses'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //load orders with given status
        $scope.loadOrders = function (status) {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'ReadOrders',
                order_status: status
            })
                    .success(function (orders) {
                        //convert time to JS Data, set collapse order content flag, check if needs deposit
                        for (i = 0; i < orders.length; i++) {
                            orders[i].time = MysqlTimeToTime(orders[i].time);
                            orders[i].deliveryTime = MysqlTimeToTime(orders[i].deliveryTime);
                            orders[i].expanded = false;
                            $scope.checkOrderForDeposit(orders[i]);
                        }
                        //
                        switch (status) {
                            case $scope.orderStatuses.created:
                                $scope.ordersNew = orders;
                                break;
                            case $scope.orderStatuses.processed:
                                $scope.ordersProcessed = orders;
                                break;
                            case $scope.orderStatuses.sent:
                                $scope.ordersSent = orders;
                                break;
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot load list of orders with status ' + status
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //expand/collapse order content (items list)
        $scope.switchExpanding = function (orders, index) {
            orders[index].expanded = !orders[index].expanded;
        };

        //change order status
        $scope.changeOrderStatusClick = function (orders, index, status) {
            switch (status) {
                case $scope.orderStatuses.created:
                case $scope.orderStatuses.processed:
                case $scope.orderStatuses.sent:
                    $scope.changeOrderStatus(orders, index, status);
                    break;
                //ask for confirmation before finalize
                case $scope.orderStatuses.finished:
                    dialogParameters = {
                        header: 'Finalize Order',
                        message: 'Sure about closing order?',
                        isDoable: true
                    };
                    
                    //show confirmation dialog
                    dialog = $scope.confirmationDialog(dialogParameters);
                    
                    //change if confirmed
                    dialog.result.then(function () {
                        $scope.changeOrderStatus(orders, index, status);
                    });
                    break;
            }
        };

        //change order status and move it to another list
        $scope.changeOrderStatus = function (orders, index, status) {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'ChangeOrderStatus',
                order_id: orders[index].id,
                order_status: status
            })
                    .success(function (newStatus) {
                        switch (newStatus) {
                            case $scope.orderStatuses.created:
                                $scope.ordersNew.push(orders[index]);
                                break;
                            case $scope.orderStatuses.processed:
                                $scope.ordersProcessed.push(orders[index]);
                                break;
                            case $scope.orderStatuses.sent:
                                $scope.ordersSent.push(orders[index]);
                                break;
                        }
                        orders.splice(index, 1);
                        //if there was collision changing statuses, notify user
                        if (newStatus !== status) {
                            dialogParameters = {
                                header: 'Orders Error',
                                message: 'Status was changed by another admin'
                            };
                            $scope.errorMessageDialog(dialogParameters);
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot change order status'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //cancel order
        $scope.cancelOrderClick = function (orders, index) {
            dialogParameters = {
                header: 'Cancel Order',
                message: 'This cannot be undone. Sure about cancelling order?',
                isDoable: true
            };
            
            //show confirmation dialog
            dialog = $scope.confirmationDialog(dialogParameters);
            
            //cancel if confirmed
            dialog.result.then(function () {
                $scope.cancelOrder(orders, index);
            });
        };

        //cancel order, remove from corresponding orders list
        $scope.cancelOrder = function (orders, index) {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'CancelOrder',
                order_id: orders[index].id
            })
                    .success(function (status) {
                        orders.splice(index, 1);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot cancel order'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //***********************************************************************

        //load app settings
        $scope.loadSettings = function () {
            $http.post("/fondou_app/settings/settings_manager.php", {
                action: 'ReadSettings'
            })
                    .success(function (settings) {
                        $scope.settings = settings;
                        //check each order if it needs deposit
                        for (i = 0; i < $scope.ordersNew.length; i++) {
                            $scope.checkOrderForDeposit($scope.ordersNew[i]);
                        }
                        for (i = 0; i < $scope.ordersProcessed.length; i++) {
                            $scope.checkOrderForDeposit($scope.ordersProcessed[i]);
                        }
                        for (i = 0; i < $scope.ordersSent.length; i++) {
                            $scope.checkOrderForDeposit($scope.ordersSent[i]);
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Settings Error',
                            message: 'Cannot load settings'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //check order if it needs deposit, set flag
        $scope.checkOrderForDeposit = function (order) {
            if ($scope.settings !== null) {
                if (order.advanced) {
                    order.needDeposit = true;
                } else {
                    if (order.totalPrice > $scope.settings.maxNoDepositCostForSimpleOrder) {
                        order.needDeposit = true;
                    } else {
                        order.needDeposit = false;
                    }
                }
            }
        };

        //***********************************************************************

        //initialize page
        $scope.initialize();
        $scope.loadSettings();

    }]);