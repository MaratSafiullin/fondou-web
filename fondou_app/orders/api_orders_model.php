<?php

/**
 * Class to implement order data model. Client's point of view
 */
class ApiOrdersModel {

    /**
     * Create (post) new order.
     * Throw exception if order is not valid or posting failed
     * @param $mysqlLink Link to DB connection
     * @param $customerId Customer Id
     * @param $order Order-to-post object (JSON encoded) from client
     * @return \PostedOrder Posted order object, contains order items list
     */
    function createOrder($mysqlLink, $customerId, $order) {
        //get allowed statuses
        $ordersModel = new OrdersModel;
        $statuses = $ordersModel->getOrderStatuses();
        //get app settings
        $settingsModel = new ApiSettingsModel;
        $settings = $settingsModel->getSettings($mysqlLink);
        //decode JSON
        $order = json_decode($order);
        //check if valid
        $this->checkOrderValidity($order, $settings);
        //calculate/set delivery time
        if ($order->advanced) {
            $advanced = 1;
            $orderTime = strtotime($order->orderTime);
            $deliveryTime = strtotime($order->deliveryTime);
        } else {
            $advanced = 0;
            $orderTime = strtotime($order->orderTime);
            $timeFrame = (integer) $order->timeFrame;
            $deliveryTime = $orderTime + $timeFrame * 60;
        }
        $address = $order->address;
        //create new order record
        $orderTimeStr = date('Y-m-d H:i:s', $orderTime);
        $deliveryTimeStr = date('Y-m-d H:i:s', $deliveryTime);
        $status = $statuses->created;
        $query = "INSERT INTO tbl_orders (customer_id, time, delivery_time, address, advanced, status) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "isssis", $customerId, $orderTimeStr, $deliveryTimeStr, $address, $advanced, $status);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        //
        $orderId = mysqli_insert_id($mysqlLink);
        //create order item records, calc total price
        $totalPrice = 0;
        $itemsCount = 0;
        foreach ($order->items as $item) {
            //get menu item from DB matching order item info from client AND active 
            $query = "SELECT itm.name, itm.price, sz.name, itm.size_id "
                    . "FROM tbl_menu_items AS itm "
                    . "INNER JOIN tbl_sizes AS sz ON sz.order = itm.size_id "
                    . "WHERE itm.id = ? AND itm.active = 1";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "i", $item->id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $itemName, $itemPrice, $itemSize, $itemSizeId);
            mysqli_stmt_store_result($stmt);
            $numRows = $stmt->num_rows;
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);
            //if matching record found
            if ($numRows > 0) {
                //create order item name
                if ($itemSizeId !== 0) {
                    $itemName = $itemName . " " . $itemSize;
                }
                //create order item description out of options and their values of the order item info from client 
                $itemDescription = '';
                foreach ($item->options as $option) {
                    //find matching option-value pair
                    $query = "SELECT opt.name, val.value, val.price_modifier "
                            . "FROM tbl_options AS opt "
                            . "INNER JOIN tbl_options_values AS val ON val.option_id = opt.id "
                            . "WHERE opt.id = ? AND val.id = ?";
                    $stmt = mysqli_prepare($mysqlLink, $query);
                    mysqli_stmt_bind_param($stmt, "ii", $option->optionId, $option->valueId);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_bind_result($stmt, $optionName, $valueName, $priceModifier);
                    mysqli_stmt_store_result($stmt);
                    $numRows = $stmt->num_rows;
                    mysqli_stmt_fetch($stmt);
                    mysqli_stmt_close($stmt);
                    //if found add text to description, modify item price
                    if ($numRows > 0) {
                        if (mb_strlen($itemDescription) > 0) {
                            $itemDescription .= " | {$optionName} = {$valueName}";
                        } else {
                            $itemDescription = "{$optionName} = {$valueName}";
                        }
                        $itemPrice += $priceModifier;
                    }
                }
                //create new order item record
                $query = "INSERT INTO tbl_orders_items (order_id, count, name, description, price) VALUES (?, ?, ?, ?, ?)";
                $stmt = mysqli_prepare($mysqlLink, $query);
                mysqli_stmt_bind_param($stmt, "iissi", $orderId, $item->count, $itemName, $itemDescription, $itemPrice);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                //
                $totalPrice += ($item->count * $itemPrice);
                $itemsCount++;
            }
        }

        //if no items were added to the order, delete it and throw Exception 
        if ($itemsCount === 0) {
            $query = "DELETE FROM tbl_orders WHERE id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "i", $orderId);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            throw new Exception('Empty order');
        } 
        //otherwise update total price
        else {
            $query = "UPDATE tbl_orders SET total_price = ? WHERE id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "ii", $totalPrice, $orderId);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
        }

        //read new order
        $newOrder = $this->readPostedOrder($mysqlLink, $orderId, $customerId);
        
        return $newOrder;
    }

    /**
     * Check if order-to-post object from client is valid. Meets timing and items presence requirements
     * Throw exception if not valid 
     * @param $order Order-to-post object
     * @param $settings App settings object
     */
    function checkOrderValidity($order, $settings) {
        //get time constraints for this order type
        if ($order->advanced) {
            $advanced = 1;
            $orderTime = strtotime($order->orderTime);
            $deliveryTime = strtotime($order->deliveryTime);
        } else {
            $advanced = 0;
            $orderTime = strtotime($order->orderTime);
            $timeFrame = (integer) $order->timeFrame;
            $deliveryTime = $orderTime + $timeFrame * 60;
        }

        //check if has items
        if (count($order->items) === 0) {
            throw new Exception('Order has no items');
        }
        //for advanced order
        if ($advanced) {
            //check timeframe (days)
            $dateDiff = date_diff(new DateTime(date('Y-m-d', $orderTime)), new DateTime(date('Y-m-d', $deliveryTime)))->days;
            if ($dateDiff < $settings->minimumTimeFrameAdvanced) {
                throw new Exception('Time frame is less than minimal');
            }
            //check pickup/delivery time validity
            $deliveryDate = strtotime(date('Y-m-d', $deliveryTime));
            $startTime = $deliveryDate + $settings->orderingStartHour * 60 * 60 + $settings->orderingStartMinute * 60;
            $finishTime = $deliveryDate + $settings->orderingFinishHour * 60 * 60 + $settings->orderingFinishMinute * 60;
            if ($deliveryTime < $startTime) {
                throw new Exception('Delivery time is too early');
            }
            if ($deliveryTime > $finishTime) {
                throw new Exception('Delivery time is too late');
            }
        }
        //for simple order
        else {
            //check timeframe (days)
            if ($timeFrame < $settings->minimumTimeFrameSimple) {
                throw new Exception('Time frame is less than minimal');
            }
            //check pickup/delivery time validity
            $orderDate = strtotime(date('Y-m-d', $orderTime));
            $startTime = $orderDate + $settings->orderingStartHour * 60 * 60 + $settings->orderingStartMinute * 60;
            $finishTime = $orderDate + $settings->orderingFinishHour * 60 * 60 + $settings->orderingFinishMinute * 60;
            if ($orderTime < $startTime) {
                throw new Exception('Order time is too early');
            }
            if ($deliveryTime > $finishTime) {
                throw new Exception('Delivery time is too late');
            }
        }
    }

    /**
     * Read (created) posted order
     * @param $mysqlLink Link to DB connection
     * @param $id Order record Id
     * @param $customerId Customer Id
     * @return \PostedOrder Posted order object, contains order items list
     */
    function readPostedOrder($mysqlLink, $id, $customerId) {
        $query = "SELECT id, time, delivery_time, address, advanced, status, total_price "
                . "FROM tbl_orders "
                . "WHERE id = ? AND customer_id = ?";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "ii", $id, $customerId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $orderTime, $deliveryTime, $address, $advanced, $status, $totalPrice);
        mysqli_stmt_store_result($stmt);
        $numRows = $stmt->num_rows;
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        if ($numRows > 0) {
            $advanced = (boolean) $advanced;
            $order = new PostedOrder;
            $order->id = $id;
            $order->orderTime = $orderTime;
            $order->deliveryTime = $deliveryTime;
            $order->address = $address;
            $order->advanced = $advanced;
            $order->status = $status;
            $order->totalPrice = $totalPrice;

            //get items list
            $query = "SELECT id, count, name, description, price "
                    . "FROM tbl_orders_items "
                    . "WHERE order_id = ?";
            $stmt = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmt, "i", $order->id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id, $count, $name, $description, $price);
            mysqli_stmt_store_result($stmt);
            while (mysqli_stmt_fetch($stmt)) {
                $item = new PostedOrderItem;
                $item->id = $id;
                $item->count = $count;
                $item->name = $name;
                $item->description = $description;
                $item->price = $price;
                $order->items[] = $item;
            }
            mysqli_stmt_close($stmt);
        } else {
            throw new Exception('No order to read');
        }

        return $order;
    }

    /**
     * Get fresh orders info for client app for a given customer. 
     * All related record that have been chanaged since given time.
     * Lists of IDs of all related records to check if somethig has been deleted since given time.
     * @param $mysqlLink Link to DB connection
     * @param $customerId Customer Id
     * @param $timestamp Time of previous refresh
     * @return \OrdersList Orders info object (fresh orders info) 
     */
    function getCustomerOrdersList($mysqlLink, $customerId, $timestamp) {
        //get allowed order statuses
        $ordersModel = new OrdersModel;
        $statuses = $ordersModel->getOrderStatuses();

        $result = new OrdersList();
        //timestamp for orders info (time of actualization)
        //----------------------------------------------------------------------
        $query = "SELECT CURRENT_TIMESTAMP";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $newTimestamp);
        mysqli_stmt_store_result($stmt);
        mysqli_stmt_fetch($stmt);
        $result->timestamp = $newTimestamp;
        //list of order record IDs
        //----------------------------------------------------------------------
        $query = "SELECT id FROM tbl_orders WHERE customer_id = ? AND status IN (?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "isss", $customerId, $statuses->created, $statuses->processed, $statuses->sent);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->orderIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //list of order item record IDs
        //----------------------------------------------------------------------
        $query = "SELECT itm.id FROM tbl_orders_items AS itm "
                . "INNER JOIN tbl_orders AS ord ON ord.id = itm.order_id "
                . "WHERE ord.customer_id = ? AND ord.status IN (?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "isss", $customerId, $statuses->created, $statuses->processed, $statuses->sent);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_store_result($stmt);
        while (mysqli_stmt_fetch($stmt)) {
            $result->itemIds[] = $id;
        }
        mysqli_stmt_close($stmt);
        //fresh order info, each order contains order items list
        //----------------------------------------------------------------------
        $query = "SELECT id, time, delivery_time, address, advanced, status, total_price "
                . "FROM tbl_orders "
                . "WHERE customer_id = ? AND timestamp > ? AND status IN (?, ?, ?)";
        $stmt = mysqli_prepare($mysqlLink, $query);
        mysqli_stmt_bind_param($stmt, "issss", $customerId, $timestamp, $statuses->created, $statuses->processed, $statuses->sent);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $orderTime, $deliveryTime, $address, $advanced, $status, $totalPrice);
        mysqli_stmt_store_result($stmt);

        while (mysqli_stmt_fetch($stmt)) {
            $advanced = (boolean) $advanced;
            $order = new PostedOrder;
            $order->id = $id;
            $order->orderTime = $orderTime;
            $order->deliveryTime = $deliveryTime;
            $order->address = $address;
            $order->advanced = $advanced;
            $order->status = $status;
            $order->totalPrice = $totalPrice;

            //get items for the order
            $query = "SELECT id, count, name, description, price "
                    . "FROM tbl_orders_items "
                    . "WHERE order_id = ?";
            $stmtItem = mysqli_prepare($mysqlLink, $query);
            mysqli_stmt_bind_param($stmtItem, "i", $order->id);
            mysqli_stmt_execute($stmtItem);
            mysqli_stmt_bind_result($stmtItem, $id, $count, $name, $description, $price);
            mysqli_stmt_store_result($stmtItem);
            while (mysqli_stmt_fetch($stmtItem)) {
                $item = new PostedOrderItem;
                $item->id = $id;
                $item->count = $count;
                $item->name = $name;
                $item->description = $description;
                $item->price = $price;
                $order->items[] = $item;
            }
            mysqli_stmt_close($stmtItem);
            $result->orders[] = $order;
        }
        mysqli_stmt_close($stmt);
        //----------------------------------------------------------------------
        return $result;
    }

    /**
     * Cancel order (by customer). 
     * @param $mysqlLink Link to DB connection
     * @param $id Order record Id
     * @param $customerId Customer Id
     * @return \CancelOrderResult Response object for client app (cancel result text)
     */
    function cancelOrder($mysqlLink, $id, $customerId) {
        $ordersModel = new OrdersModel;
        $finalStatuses = $ordersModel->getOrderFinalStatuses();

        $result = new CancelOrderResult;
        try {
            $order = $this->readPostedOrder($mysqlLink, $id, $customerId);
            if ($order != null) {
                $newStatus = $ordersModel->cancelOrder($mysqlLink, $id, $finalStatuses->canceledCustomer);
                if ($newStatus === $finalStatuses->canceledCustomer) {
                    $result->result = 'SUCCESS';
                } else {
                    $result->result = 'FAIL';
                }
            }
        } catch (Exception $e) {
            $result->result = 'FAIL';
        }
        return $result;
    }

}

/**
 * Class - response object for client app with fresh customer orders info
 */
class OrdersList {

    public $timestamp;
    public $orders = array();
    public $orderIds = array();
    public $itemIds = array();

}

/**
 * Class - order record for client app
 */
class PostedOrder {

    public $id;
    public $orderTime;
    public $deliveryTime;
    public $address;
    public $advanced;
    public $status;
    public $totalPrice;
    public $items = array();

}

/**
 * Class - order item record for client app
 */
class PostedOrderItem {

    public $id;
    public $count;
    public $name;
    public $description;
    public $price;

}

/**
 * Class - cancel order response for client app
 */
class CancelOrderResult {

    public $result;

}
