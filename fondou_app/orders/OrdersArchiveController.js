//Controller for orders archive page
app.controller('OrdersArchiveController', ['$scope', '$http', function ($scope, $http) {
        //default max count or orders in page
        $scope.pageSize = 5;
        //possible final order statuses list
        $scope.orderFinalStatuses = [];
        //page of finished orders (subset of the whole list)
        $scope.ordersFinished = [];
        //page starting row
        $scope.ordersFinishedStartRow = 0;
        //page orders count (actual)
        $scope.ordersFinishedCount = 0;
        //page of orders cancelled by admin (subset of the whole list)
        $scope.ordersCanceledAdmin = [];
        //page starting row
        $scope.ordersCanceledAdminStartRow = 0;
        //page orders count (actual)
        $scope.ordersCanceledAdminCount = 0;
        //page of orders cancelled by customer (subset of the whole list)
        $scope.ordersCanceledCustomer = [];
        //page starting row
        $scope.ordersCanceledCustomerStartRow = 0;
        //page orders count (actual)
        $scope.ordersCanceledCustomerCount = 0;

        //***********************************************************************

        //load final statuses and 3 orders pages (starting from the beginning of the whole list)
        $scope.initialize = function () {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'ReadFinalStatuses'
            })
                    .success(function (statusesList) {
                        $scope.orderFinalStatuses = statusesList;
                        //load pages
                        $scope.loadOrdersPage($scope.orderFinalStatuses.finished, $scope.ordersFinishedStartRow, $scope.pageSize);
                        $scope.loadOrdersPage($scope.orderFinalStatuses.canceledAdmin, $scope.ordersCanceledAdminStartRow, $scope.pageSize);
                        $scope.loadOrdersPage($scope.orderFinalStatuses.canceledCustomer, $scope.ordersCanceledCustomerStartRow, $scope.pageSize);
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot load statuses'
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //load orders page, set page orders count
        $scope.loadOrdersPage = function (status, startRow, rowCount) {
            $http.post("/fondou_app/orders/orders_manager.php", {
                action: 'ReadOrdersPage',
                order_status: status,
                start_row: startRow,
                row_count: rowCount
            })
                    .success(function (ordersPage) {
                        //convert time to JS Data, set collapse order content flag
                        for (i = 0; i < ordersPage.ordersList.length; i++) {
                            ordersPage.ordersList[i].time = MysqlTimeToTime(ordersPage.ordersList[i].time);
                            ordersPage.ordersList[i].deliveryTime = MysqlTimeToTime(ordersPage.ordersList[i].deliveryTime);
                            ordersPage.ordersList[i].expanded = false;
                        }
                        //
                        switch (status) {
                            case $scope.orderFinalStatuses.finished:
                                $scope.ordersFinishedCount = ordersPage.count;
                                $scope.ordersFinished = ordersPage.ordersList;
                                break;
                            case $scope.orderFinalStatuses.canceledAdmin:
                                $scope.ordersCanceledAdminCount = ordersPage.count;
                                $scope.ordersCanceledAdmin = ordersPage.ordersList;
                                break;
                            case $scope.orderFinalStatuses.canceledCustomer:
                                $scope.ordersCanceledCustomerCount = ordersPage.count;
                                $scope.ordersCanceledCustomer = ordersPage.ordersList;
                                break;
                        }
                    })
                    .error(function (err) {
                        dialogParameters = {
                            header: 'Orders Error',
                            message: 'Cannot load list of orders with status ' + status
                        };
                        $scope.errorMessageDialog(dialogParameters);
                    });
        };

        //expand/collapse order content (items list)
        $scope.switchExpanding = function (orders, index) {
            orders[index].expanded = !orders[index].expanded;
        };

        //switch to other page (next or previous) if possible (haven't reached the limit)
        $scope.switchPage = function (status, isNext) {
            switch (status) {
                case $scope.orderFinalStatuses.finished:
                    if (isNext) {
                        if ($scope.ordersFinishedStartRow + $scope.pageSize < $scope.ordersFinishedCount) {
                            $scope.ordersFinishedStartRow = $scope.ordersFinishedStartRow + $scope.pageSize;
                        }
                    } else {
                        $scope.ordersFinishedStartRow = $scope.ordersFinishedStartRow - $scope.pageSize;
                        if ($scope.ordersFinishedStartRow < 0) {
                            $scope.ordersFinishedStartRow = 0;
                        }
                    }
                    $scope.loadOrdersPage($scope.orderFinalStatuses.finished, $scope.ordersFinishedStartRow, $scope.pageSize);
                    break;
                case $scope.orderFinalStatuses.canceledAdmin:
                    if (isNext) {
                        if ($scope.ordersCanceledAdminStartRow + $scope.pageSize < $scope.ordersCanceledAdminCount) {
                            $scope.ordersCanceledAdminStartRow = $scope.ordersCanceledAdminStartRow + $scope.pageSize;
                        }
                    } else {
                        $scope.ordersCanceledAdminStartRow = $scope.ordersCanceledAdminStartRow - $scope.pageSize;
                        if ($scope.ordersCanceledAdminStartRow < 0) {
                            $scope.ordersCanceledAdminStartRow = 0;
                        }
                    }
                    $scope.loadOrdersPage($scope.orderFinalStatuses.canceledAdmin, $scope.ordersCanceledAdminStartRow, $scope.pageSize);
                    break;
                case $scope.orderFinalStatuses.canceledCustomer:
                    if (isNext) {
                        if ($scope.ordersCanceledCustomerStartRow + $scope.pageSize < $scope.ordersCanceledCustomerCount) {
                            $scope.ordersCanceledCustomerStartRow = $scope.ordersCanceledCustomerStartRow + $scope.pageSize;
                        }
                    } else {
                        $scope.ordersCanceledCustomerStartRow = $scope.ordersCanceledCustomerStartRow - $scope.pageSize;
                        if ($scope.ordersCanceledCustomerStartRow < 0) {
                            $scope.ordersCanceledCustomerStartRow = 0;
                        }
                    }
                    $scope.loadOrdersPage($scope.orderFinalStatuses.canceledCustomer, $scope.ordersCanceledCustomerStartRow, $scope.pageSize);
                    break;
            }
        };

        //***********************************************************************

        //initialize page
        $scope.initialize();

    }]);