<?php
include_once 'fondou_app/common/error_handler.php';
include_once 'fondou_app/common/database_access.php';
include_once 'fondou_app/common/authorization.php';

$databaseAccess = new DatabaseAccess;
$authorization = new Authorization;

//if logged in redirect to dashboard
session_start();
if (isset($_SESSION['id'])) {
    header("Location:/dashboard/");
}
//else check cookie for saved login
else {
    $mysqlLink = $databaseAccess->dbOpen();
    $loginInfo = $authorization->checkCookie($mysqlLink);
    $databaseAccess->dbClose($mysqlLink);

    //if have valid cookie
    if ($loginInfo != null) {
        //set session data
        $_SESSION['login'] = $loginInfo->login;
        $_SESSION['id'] = $loginInfo->id;
        //renew cookie
        setcookie("user_data", json_encode($loginInfo), time() + 9999999);
        //redirect to dashboard
        header("Location:/dashboard/");
    }
}
//read params
$action = filter_input(INPUT_POST, 'action');
$login = filter_input(INPUT_POST, 'login');
$password = filter_input(INPUT_POST, 'password');
$save = filter_input(INPUT_POST, 'save');

successfulResponse();
$err = 0;

//if trying to log in (page called itself)
if ($action == "login") {
    //empty email - error
    if (empty($login)) {
        $err = 2;
    }
    //or
    else {
        //check login/password
        $mysqlLink = $databaseAccess->dbOpen();
        $loginInfo = $authorization->checkLoginPassword($mysqlLink, $login, $password);
        $databaseAccess->dbClose($mysqlLink);
        //if success
        if ($loginInfo != null) {
            //set session data
            $_SESSION['login'] = $loginInfo->login;
            $_SESSION['id'] = $loginInfo->id;
            //save cookie if option is chosen
            if ($save == 1) {
                setcookie("user_data", json_encode($loginInfo), time() + 9999999);
            }
            //redirect to dashboard
            header("Location:/dashboard/");
        }
        //or log in fail
        else {
            $err = 1;
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>fond(ou) café Admin Login</title>

        <link rel="icon" href="/assets/img/favicon.ico">
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">fond(ou) Admin Login </div>
                    <div class="panel-body">
                        <?php
                        //if log in error
                        switch ($err) {
                            case 1:
                                echo "Wrong login/password";
                                break;
                            case 2:
                                echo "Empty login";
                                break;
                        }
                        ?>
                        <form role="form" action="/login.php" method="post">
                            <fieldset>
                                <input type='text' name = 'action' value = 'login' readonly hidden>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Login" name="login" type="text" autofocus="" value="<?php echo "{$login}" ?>">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="<?php echo "{$password}" ?>">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="save" type="checkbox" value="1" <?php if (($save == 1)) {echo 'checked';} ?>>Remember Me
                                    </label>
                                </div>
                                <input class="btn btn-primary" type="submit" name="submit" value="Log In">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div><!-- /.col-->
        </div><!-- /.row -->		
    </body>
</html>
