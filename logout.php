<?php

session_start();

//erase cookies
setcookie(session_name(), session_id(), 0);
setcookie("user_data", "", 0);
//close session
session_unset();
session_destroy();

//redirect to index page
header("Location:/");
exit;
?>